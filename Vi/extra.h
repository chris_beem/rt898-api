//
// Created by chris.beem on 22/01/2021.
//

#ifndef RT898VI_EXTRA_H
#define RT898VI_EXTRA_H

#include <stdbool.h>


#define UINT16 unsigned short
#define UINT8 unsigned char
#define INT8 char
#define UINT32 unsigned long
#define INT32 long
#define TRUE true
#define FALSE false
#define BOOL bool

#endif //RT898VI_EXTRA_H
