//-----------------------------------------------------//
//
// Filename: R898.c
//
// This file is R898 tuner driver
// Copyright 2020 by Rafaelmicro., Inc.
// Author: Vincent Chien
//-----------------------------------------------------//

//#include "stdafx.h"
//#include <stdafx.h>
#include "extra.h"
#include "R898.h"
#include "i2c_sys.h"    // "I2C_Sys" is only for SW porting reference.

UINT32 R898_Xtal = 24000;  //not release

#define  ADC_MULTI_READ  1
#define  R898_S_FOR_SHARP     TRUE 
UINT32 ADC_READ_DELAY = 5;
UINT8  ADC_READ_COUNT = 1;
UINT8  VGA_DELAY = 5;
UINT8  FILTER_DELAY = 2;   


R898_IntTunerNum_Type  R898_IntTunerNum = R898_TUNER;
R898_S_Initial_Type R898_S_Last_Init_Type[4] = {Init_Freq_First,Init_Freq_First,Init_Freq_First,Init_Freq_First};

UINT8  R898_S_Initial_done_flag[MAX_TUNER_NUM] = {FALSE, FALSE, FALSE, FALSE};
UINT32 R898_satellite_bw;
UINT32 R898_pre_satellite_bw[MAX_TUNER_NUM];

UINT8 R898_SetTfType_UL_MID = R898_MID_USING_39NH;   //default: DTV
UINT8 R898_DetectTfType_Cal = R898_UL_USING_270NH;  //Bead, 270n, 390n
UINT8 R898_DetectMidTfType_def = R898_MID_USING_39NH;   


UINT8   R898_SHARE_XTAL = R898_NO_SHARE_XTAL;
UINT8   R898_XTAL_CAP_VALUE = 38;    //range 0~41 normal=38
UINT8   R898_XTAL_CAP = 38;//R898_XTAL_CAP_VALUE; CHRIS BEEM

UINT8 R898_T_iniArray_special[R898_T_REG_NUM] = { //R16-R63
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F
		0x00, 0x10, 0x80, 0xD3, 0xDF, 0x03, 0x82, 0x88, 0x8E, 0xA8,
	//  0x10  0x11  0x12  0x13  0x14  0x15  0x16  0x17  0x18  0x19
		0x3A, 0x40, 0x00, 0x00, 0x04, 0x18, 0x32, 0x3C, 0x43, 0x98, 
	//  0x1A  0x1B  0x1C  0x1D  0x1E  0x1F  0x20  0x21  0x22  0x23   
		0x85, 0x41, 0x2F, 0x27, 0x8A, 0xAA, 0xEA, 0x58, 0x58, 0x08,
	//  0x24  0x25  0x26  0x27  0x28  0x29  0x2A  0x2B  0x2C  0x2D
		0xF4, 0x76, 0x3D, 0x17, 0x33, 0x76, 0x31, 0x24, 0x44, 0xD4,
	//  0x2E  0x2F  0x30  0x31  0x32  0x33  0x34  0x35  0x36  0x37
		0x10, 0x00, 0x80, 0x30, 0xA0, 0x0A, 0x84, 0x21			 };
	//  0x38  0x39  0x3A  0x3B  0x3C  0x3D  0x3E  0x3F  



UINT8 R898_S_iniArray[R898_S_REG_NUM] = {0x00, 0x00, 0x00, 0xC0, 0xB0, 0x00, 0x10, 0xBE, 0xFD, 0x3C, 
									   0xDD, 0x67, 0xE9, 0x0F, 0xE3, 0x0F, 0x80, 0x0A, 0x1C, 0xAA,
									   0x6A, 0x0E, 0x10, 0x6C, 0x3D, 0x11, 0xD3, 0x00, 0xC8, 0x09,	
									   0x4A, 0x17};




UINT8 R898_T_LPF_CAL_172MHz[R898_T_REG_NUM] = { //R16-R63
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F
		0x06, 0x10, 0x00, 0x06, 0x7B, 0xE3, 0x80, 0x02, 0xA1, 0x0C,
	//  0x10  0x11  0x12  0x13  0x14  0x15  0x16  0x17  0x18  0x19
		0x4D, 0x04, 0x80, 0x00, 0x92, 0x09, 0x32, 0x3F, 0x70, 0x01, 
	//  0x1A  0x1B  0x1C  0x1D  0x1E  0x1F  0x20  0x21  0x22  0x23   
		0x0F, 0x00, 0x23, 0x27, 0x8B, 0x99, 0xD9, 0x4A, 0x5A, 0x08,
	//  0x24  0x25  0x26  0x27  0x28  0x29  0x2A  0x2B  0x2C  0x2D
		0xF6, 0x86, 0x2F, 0x2B, 0x2F, 0xEE, 0x20, 0xF4, 0x02, 0x05,
	//  0x2E  0x2F  0x30  0x31  0x32  0x33  0x34  0x35  0x36  0x37
		0x91, 0x08, 0xE1, 0x03, 0x40, 0x00, 0xBD, 0x41			 };
	//  0x38  0x39  0x3A  0x3B  0x3C  0x3D  0x3E  0x3F  

UINT8 R898_S_LPF_CAL_172MHz[R898_S_REG_NUM] = {//160,320,544,666,817MHz

	  0x00, 0x00, 0x00, 0x80, 0xF0, 0x06, 0x19, 0xBE, 0xDC, 0x7C, 
  //  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
	  0xE5, 0x67, 0xE9, 0x0F, 0xE0, 0x0C, 0x80, 0x62, 0x19, 0xAA,
  //  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10  0x11  0x12  0x13
	  0xAA, 0x0E, 0x10, 0x6C, 0x3D, 0x11, 0xD3, 0x00, 0xC8, 0x0B,
  //  0x14  0x15  0x16  0x17  0x18  0x19  0x1A  0x1B  0x1C  0x1D
	  0x4A, 0x17};
  //  0x1E  0x1F



UINT8 R898_T_IMR_CAL[5][R898_T_REG_NUM] = {//160,320,544 644 825MHz

		{ //160
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F
		0x06, 0x10, 0x00, 0x5E, 0x7B, 0xE3, 0x80, 0x02, 0xBF, 0xFC,
	//  0x10  0x11  0x12  0x13  0x14  0x15  0x16  0x17  0x18  0x19
		0x3D, 0x0C, 0x80, 0x00, 0xAE, 0x09, 0x39, 0x3C, 0xF0, 0xF9, 
	//  0x1A  0x1B  0x1C  0x1D  0x1E  0x1F  0x20  0x21  0x22  0x23   
		0xF5, 0x10, 0x23, 0x27, 0x58, 0x00, 0x00, 0x4A, 0x5A, 0x09,
	//  0x24  0x25  0x26  0x27  0x28  0x29  0x2A  0x2B  0x2C  0x2D
		0xF4, 0x86, 0x2D, 0x2A, 0x2B, 0xEE, 0x20, 0xF4, 0x02, 0x05,
	//  0x2E  0x2F  0x30  0x31  0x32  0x33  0x34  0x35  0x36  0x37
		0x91, 0x08, 0xE1, 0x03, 0x40, 0x00, 0xBD, 0x41			 },
	//  0x38  0x39  0x3A  0x3B  0x3C  0x3D  0x3E  0x3F  

		{ //320
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F
		0x06, 0x10, 0x00, 0x5E, 0x6B, 0x63, 0x86, 0x02, 0x9F, 0xFC,
	//  0x10  0x11  0x12  0x13  0x14  0x15  0x16  0x17  0x18  0x19
		0x3D, 0x4C, 0x80, 0x00, 0xAE, 0x0D, 0x39, 0x3C, 0xF0, 0xF9, 
	//  0x1A  0x1B  0x1C  0x1D  0x1E  0x1F  0x20  0x21  0x22  0x23   
		0xF5, 0xD0, 0x23, 0x27, 0xD7, 0x55, 0x55, 0x49, 0x5A, 0x08,
	//  0x24  0x25  0x26  0x27  0x28  0x29  0x2A  0x2B  0x2C  0x2D
		0xF4, 0x86, 0x2D, 0x2A, 0x2B, 0xEE, 0x20, 0xF4, 0x02, 0x05,
	//  0x2E  0x2F  0x30  0x31  0x32  0x33  0x34  0x35  0x36  0x37
		0x91, 0x08, 0xE1, 0x03, 0x40, 0x00, 0xBD, 0x41			 },
	//  0x38  0x39  0x3A  0x3B  0x3C  0x3D  0x3E  0x3F  

		{ //544
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F
		0x06, 0x10, 0x00, 0x5E, 0x5B, 0x63, 0x89, 0x02, 0x9F, 0xFC,
	//  0x10  0x11  0x12  0x13  0x14  0x15  0x16  0x17  0x18  0x19
		0x3D, 0x4C, 0x80, 0x00, 0xAE, 0x05, 0x39, 0x3C, 0xF0, 0xF9, 
	//  0x1A  0x1B  0x1C  0x1D  0x1E  0x1F  0x20  0x21  0x22  0x23   
		0xF5, 0xD0, 0x23, 0x27, 0xAA, 0x00, 0x00, 0x09, 0x5A, 0x09,
	//  0x24  0x25  0x26  0x27  0x28  0x29  0x2A  0x2B  0x2C  0x2D
		0xF4, 0x86, 0x2D, 0x2A, 0x2B, 0xEE, 0x20, 0xF4, 0x02, 0x05,
	//  0x2E  0x2F  0x30  0x31  0x32  0x33  0x34  0x35  0x36  0x37
		0x91, 0x08, 0xE1, 0x03, 0x40, 0x00, 0xBD, 0x41			 },
	//  0x38  0x39  0x3A  0x3B  0x3C  0x3D  0x3E  0x3F  
		{ //666
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x06, 0x10, 0x00, 0x5E, 0x5B, 0x63, 0x82, 0x88, 0x9F, 0xFC,
		0x3D, 0x4C, 0x80, 0x00, 0xAE, 0x05, 0x39, 0x3C, 0xF0, 0xF9, 
		0xF5, 0xD0, 0x23, 0x27, 0x98, 0x55, 0xD5, 0x48, 0x5A, 0x08,
		0xF4, 0x86, 0x2D, 0x2A, 0x2B, 0xEE, 0x20, 0xF4, 0x02, 0x05,
		0x91, 0x08, 0xE1, 0x03, 0x40, 0x00, 0xBD, 0x41			 },
		{ //817
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F
		0x06, 0x10, 0x00, 0x5E, 0x5B, 0x63, 0x80, 0x88, 0xFF, 0xFC,
	//  0x10  0x11  0x12  0x13  0x14  0x15  0x16  0x17  0x18  0x19
		0x3D, 0x4C, 0x80, 0x01, 0xAE, 0x05, 0x39, 0x3C, 0xF0, 0xF9, 
	//  0x1A  0x1B  0x1C  0x1D  0x1E  0x1F  0x20  0x21  0x22  0x23   
		0xF5, 0xD0, 0x23, 0x27, 0x5F, 0x55, 0x55, 0x08, 0x5A, 0x08,
	//  0x24  0x25  0x26  0x27  0x28  0x29  0x2A  0x2B  0x2C  0x2D
		0xF4, 0x86, 0x2D, 0x2A, 0x2B, 0xEE, 0x20, 0xF4, 0x02, 0x05,
	//  0x2E  0x2F  0x30  0x31  0x32  0x33  0x34  0x35  0x36  0x37
		0x91, 0x08, 0xE1, 0x03, 0x40, 0x00, 0xBD, 0x41			 },
	//  0x38  0x39  0x3A  0x3B  0x3C  0x3D  0x3E  0x3F  


};

UINT8 R898_S_IMR_CAL[5][R898_S_REG_NUM] = {//160,320,544,666 825MHz

	 {//160
	  0x00, 0x00, 0x00, 0x80, 0xF0, 0x00, 0x18, 0xBE, 0xDC, 0x5C, 
  //  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
	  0xE5, 0x67, 0xE9, 0x0F, 0xE0, 0x0C, 0x80, 0xE2, 0x18, 0xAA,
  //  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10  0x11  0x12  0x13
	  0xAA, 0x0E, 0x10, 0x6C, 0x3D, 0x11, 0xD3, 0x00, 0xC8, 0x0B,
  //  0x14  0x15  0x16  0x17  0x18  0x19  0x1A  0x1B  0x1C  0x1D
	  0x4A, 0x17},
  //  0x1E  0x1F

	  {//320
	  0x00, 0x00, 0x00, 0x80, 0xF0, 0x00, 0x18, 0xBE, 0xDC, 0x5C, 
  //  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
	  0xE5, 0x67, 0xE9, 0x0F, 0xE0, 0x0C, 0x80, 0x1A, 0x18, 0xAA,
  //  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10  0x11  0x12  0x13
	  0xAA, 0x0E, 0x10, 0x6C, 0x3D, 0x11, 0xD3, 0x00, 0xC8, 0x0B,
  //  0x14  0x15  0x16  0x17  0x18  0x19  0x1A  0x1B  0x1C  0x1D
	  0x4A, 0x17},
  //  0x1E  0x1F

	  {//544
	  0x00, 0x00, 0x00, 0x80, 0xF0, 0x00, 0x18, 0xBE, 0xDC, 0x5C, 
  //  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
	  0xE5, 0x67, 0xE9, 0x0F, 0xE0, 0x0C, 0x80, 0xD2, 0x13, 0xAA,
  //  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10  0x11  0x12  0x13
	  0x2A, 0x0E, 0x10, 0x6C, 0x3D, 0x11, 0xD3, 0x00, 0xC8, 0x0B,
  //  0x14  0x15  0x16  0x17  0x18  0x19  0x1A  0x1B  0x1C  0x1D
	  0x4A, 0x17},
  //  0x1E  0x1F
  	  {//666
	  0x00, 0x00, 0x00, 0x80, 0xF0, 0x00, 0x18, 0xBE, 0xDC, 0x5C, 
	  0xE5, 0x67, 0xE9, 0x0F, 0xE0, 0x0C, 0x80, 0xD2, 0x18, 0x00,
	  0x80, 0x0E, 0x10, 0x6C, 0x3D, 0x11, 0xD3, 0x00, 0xC8, 0x0B,
	  0x4A, 0x17},
	  {//817
	  0x00, 0x00, 0x00, 0x80, 0xF0, 0x00, 0x18, 0xBE, 0xDC, 0x5C, 
  //  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
	  0xE5, 0x67, 0xE9, 0x0F, 0xE0, 0x0C, 0x80, 0x0A, 0x0E, 0x00,
  //  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10  0x11  0x12  0x13
	  0x80, 0x0E, 0x10, 0x6C, 0x3D, 0x11, 0xD3, 0x00, 0xC8, 0x0B,
  //  0x14  0x15  0x16  0x17  0x18  0x19  0x1A  0x1B  0x1C  0x1D
	  0x4A, 0x17},
  //  0x1E  0x1F

};


UINT32 R898_S_BandWidth[59]= {50000, 73000, 96000, 104000, 116000, 126000, 136000, 148000, 155000, 170000,
							176000, 200000, 210000, 226000, 232000, 248000, 256000, 276000, 298000, 318000,
							342000, 366000, 390000, 402000, 420000, 440000, 464000, 484000, 504000, 526000,
							548000, 560000, 578000, 594000, 618000, 636000, 656000, 672000, 696000, 706000,
							716000, 740000, 760000, 776000, 794000, 814000, 832000, 854000, 878000, 900000,
							918000, 936000, 960000, 980000, 998000, 1018000, 1036000, 1050000, 1078000};

UINT16 R898_S_Coras_tune[59]=	{1, 1, 1, 1, 2, 2, 3, 3, 4, 4,
							 5, 5, 6, 6, 7, 7, 10, 10, 11, 12,
							 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
							 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
							 33, 34, 35, 36, 37, 38, 39, 41, 42, 43,
							 44, 45, 46, 47, 50, 51, 52, 53, 54};


UINT8 R898_S_Fine_tune[59]=	{0, 0, 0, 2, 0, 2, 0, 2, 0, 2,
							 0, 2, 0, 2, 0, 2, 0, 2, 2, 2,
							 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
							 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
							 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
							 2, 2, 2, 2, 2, 2, 2, 2, 2};



static UINT8 R898_S_LNA_Index[32] = 
{
  0, 0, 0, 0, 0, 0, 0, 0, 0, 1,  //0~9
  2, 3, 4, 5, 6, 7, 8, 9,10,11,  //10~19
 12,13,14,15,16,16,16,17,18,18,  //20~29
 18,18,                          //30~31
};

static UINT8 R898_S_RF_Index[32] = 
{
 0, 0, 0, 0, 0, 0, 0, 1, 2, 2,  //0~9
 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,	//10~19
 2, 2, 2, 2, 2, 2, 2, 2, 2, 3,	//20~29
 4, 5,							//30~31
};

static UINT8 R898_S_Mixer_Index[32] = 
{
 0, 1, 2, 3, 4, 5, 6, 6, 6, 6,  //0~9
 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,	//10~19
 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,	//20~29
 6, 6,							//30~31
};


UINT8   R898_ADDRESS[2]    = {0xE6, 0xFA};//T:S
UINT8   R898_REG_NUMBER[2] = {64, 32};//T:S
UINT8   R898_S_R1TOR4_Write_Arry[4]={0x00,0x00,0x00,0x00};


UINT8 R898_XtalDiv = XTAL_DIV2;
UINT8 R898_DetectTfType = R898_UL_USING_270NH;
UINT8 R898_Fil_Cal_Gap = 8;
UINT32 R898_IF_HIGH = 8500;  
UINT8 R898_Xtal_Pwr[MAX_TUNER_NUM] = {XTAL_SMALL_HIGHEST, XTAL_SMALL_HIGHEST, XTAL_SMALL_HIGHEST, XTAL_SMALL_HIGHEST, XTAL_SMALL_HIGHEST, XTAL_SMALL_HIGHEST};
UINT8 R898_Xtal_Pwr_tmp[MAX_TUNER_NUM] = {XTAL_LARGE_HIGHEST, XTAL_LARGE_HIGHEST, XTAL_LARGE_HIGHEST, XTAL_LARGE_HIGHEST, XTAL_LARGE_HIGHEST, XTAL_LARGE_HIGHEST};

//----------------------------------------------------------//
//                   Internal Parameters                    //
//----------------------------------------------------------//


UINT8 R898_T_Array[R898_T_REG_NUM];
UINT8 R898_S_Array[R898_S_REG_NUM];


R898_SectType R898_IMR_Data[MAX_TUNER_NUM][5]= {{0,0,0,0,0}};//Please keep this array data for standby mode.
//R898_SectType R898_IMR_Data[MAX_TUNER_NUM][5];//Please keep this array data for standby mode.
I2C_TYPE  R898_I2C;
I2C_LEN_TYPE R898_I2C_Len;
I2C_LEN_TYPE Test_Len;



UINT8  R898_IMR_point_num;
UINT8  R898_Initial_done_flag[MAX_TUNER_NUM] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};

UINT8  R898_T_SBY_FLAG[MAX_TUNER_NUM] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};
UINT8  R898_S_SBY_FLAG[MAX_TUNER_NUM] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};


UINT8  R898_Initial_xtal_check_flag[MAX_TUNER_NUM] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};
UINT8  R898_IMR_done_flag[MAX_TUNER_NUM] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};
UINT8  R898_Bandwidth[MAX_TUNER_NUM] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
UINT8  R898_SATELLITE_FLAG[MAX_TUNER_NUM] = {0, 0, 0, 0, 0, 0};
UINT8  R898_Fil_Cal_flag[MAX_TUNER_NUM][R898_STD_SIZE];
static UINT8 R898_Fil_Cal_BW[MAX_TUNER_NUM][R898_STD_SIZE];
static UINT8 R898_Fil_Cal_code[MAX_TUNER_NUM][R898_STD_SIZE];
static UINT8 R898_Fil_Cal_LpfLsb[MAX_TUNER_NUM][R898_STD_SIZE];    
static UINT8 R898_IMR_Cal_Type = R898_IMR_CAL;
static R898_Standard_Type R898_pre_standard[MAX_TUNER_NUM] = {R898_STD_SIZE, R898_STD_SIZE, R898_STD_SIZE, R898_STD_SIZE, R898_STD_SIZE, R898_STD_SIZE};
//static UINT32 R898_pre_satellite_bw;


static UINT8 R898_S_SBY[MAX_TUNER_NUM][R898_S_REG_NUM];
static UINT8 R898_T_SBY[MAX_TUNER_NUM][R898_T_REG_NUM];

//0x00(8M), 0x40(7M), 0x60(6M)
static UINT8 R898_Fil_Cal_BW_def[R898_STD_SIZE]={
       0x60, 0x60, 0x00, 0x00, 0x40, 0x40, 0x00, 0x40, 0x00, //ATV
	   0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,       //ATV (CIF 5M)
	   0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x00,       //DVB-T, DVB-T2
	   0x00, 0x40, 0x40, 0x40, 0x40, 0x00, 0x00, 0x40,		 //DVBC_8M, DVBC_6M, J83B, ISDBT_4063, ISDBT_4570, DTMB_4570, DTMB_6000, DTMB_6M_BW_IF_5M
	   0x40, 0x40, 0x40,									 //DTMB_6M_4500, ATSC, Satellite(don't care), 
	   0x40, 0x00, 0x00, 0x40, 0x00, 0x00, 0x60,             //DVB-T, DVB-T2 (IF 5M)
	   0x00, 0x40, 0x40, 0x40, 0x00, 0x40, 0x40        //DVB-C, J83B, ISDBT, DTMB, ATSC (IF 5M), FM
       };
static UINT8 R898_Fil_Cal_code_def[R898_STD_SIZE]={
       9, 2, 4, 5, 2, 2, 5, 0, 5,     //ATV
       2, 2, 3, 5, 3, 3, 3, 3,        //ATV (CIF 5M)
       6, 2, 0, 6, 2, 0, 2, 2,        //DVB-T, DVB-T2
	   2, 1, 2, 7, 6, 6, 1, 1,		  //DVB-C, J83B, ISDBT, DTMB
	   7, 1, 1,					      //DTMB_6M_4500, ATSC, Satellite(don't care), 
	   4, 6, 5, 4, 6, 5, 11,          //DVB-T, DVB-T2 (IF 5M)
	   2, 0, 2, 3, 4, 1, 6         //DVB-C, J83B, ISDBT, DTMB, ATSC (IF 5M), FM
       };

static UINT8 R898_IMR_Cal_Result[MAX_TUNER_NUM] = {0, 0, 0, 0, 0, 0};  //1: fail, 0: ok
static UINT8 R898_TF_Check_Result[MAX_TUNER_NUM] = {0, 0, 0, 0, 0, 0}; //1: fail, 0: ok




UINT32 R898_LNA_MID_LOW[R898_TF_SIZE] = { 236000, 164000, 292000, 236000, 236000, 220000};
UINT32 R898_LNA_HIGH_MID[R898_TF_SIZE] = { 508000, 484000, 620000, 620000, 620000, 508000}; 




UINT32 R898_TF_Freq_High[R898_TF_SIZE][R898_TF_HIGH_NUM] = 
{  
	 { 720000, 696000, 664000, 624000, 608000, 560000, 544000, 512000},	 
     { 720000, 664000, 624000, 608000, 560000, 528000, 512000, 488000},
	 { 720000, 704000, 688000, 672000, 656000, 528000, 640000, 624000},
	 { 720000, 704000, 688000, 672000, 656000, 528000, 640000, 624000},
	 { 720000, 704000, 688000, 672000, 656000, 528000, 640000, 624000},
	 { 720000, 696000, 664000, 624000, 608000, 560000, 544000, 512000}
};
UINT32 R898_TF_Freq_Mid[R898_TF_SIZE][R898_TF_MID_NUM] = 
{	 
	  {400000, 384000, 368000, 352000, 336000, 320000, 304000, 288000, 272000, 256000, 240000}, //270n/39n
	  {320000, 304000, 272000, 264000, 240000, 233000, 216000, 200000, 192000, 184000, 168000}, //Bead/68n 
	  {496000, 464000, 448000, 416000, 400000, 384000, 368000, 352000, 336000, 320000, 296000}, //270n/27n  
	  {496000, 464000, 448000, 416000, 400000, 368000, 336000, 304000, 288000, 272000, 240000}, //Bead/27n
	  {496000, 464000, 448000, 416000, 400000, 368000, 336000, 304000, 288000, 272000, 240000}, //390n/27n
	  {400000, 384000, 368000, 352000, 320000, 304000, 288000, 272000, 256000, 240000, 224000}  //390n/39n
};
UINT32 R898_TF_Freq_Low[R898_TF_SIZE][R898_TF_LOW_NUM] = 
{    
	  {195000, 176000, 144000, 128000, 112000, 96000, 80000, 64000, 48000},  //270n/39n
      {168000, 160000, 144000, 128000, 112000, 96000, 64000, 56000, 48000},  //Bead/68n 
	  {200000, 176000, 144000, 128000, 112000, 96000, 80000, 64000, 48000},  //270n/27n 
	  {168000, 160000, 144000, 128000, 112000, 96000, 64000, 56000, 48000},  //Bead/27n 
	  {144000, 136000, 128000, 104000, 96000, 80000, 64000, 56000, 48000},   //390n/27n
	  {144000, 136000, 128000, 104000, 96000, 80000, 64000, 56000, 48000}    //390n/39n
};


UINT8 R898_TF_Result_High[R898_TF_SIZE][R898_TF_HIGH_NUM] = 
{    
	  //{0x00, 0x01, 0x02, 0x04, 0x04, 0x07, 0x09, 0x0B},
	  {0x00, 0x01, 0x02, 0x04, 0x09, 0x0C, 0x0E, 0x0F},
	  {0x00, 0x02, 0x04, 0x04, 0x07, 0x0A, 0x0D, 0x0E},
	  {0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x04},
	  {0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x04},
	  {0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x03, 0x04},
	  {0x00, 0x01, 0x02, 0x04, 0x04, 0x07, 0x09, 0x0B}
};
UINT8 R898_TF_Result_Mid[R898_TF_SIZE][R898_TF_MID_NUM] = 
{    
	  {0x00, 0x01, 0x03, 0x03, 0x04, 0x05, 0x07, 0x08, 0x0B, 0x0D, 0x0F},
      {0x00, 0x02, 0x04, 0x04, 0x07, 0x0A, 0x0D, 0x0E, 0x12, 0x12, 0x16},
	  {0x00, 0x02, 0x02, 0x04, 0x04, 0x06, 0x07, 0x09, 0x0B, 0x0C, 0x0F},
      {0x00, 0x02, 0x02, 0x04, 0x04, 0x07, 0x0B, 0x0E, 0x11, 0x13, 0x18},
	  {0x00, 0x02, 0x02, 0x04, 0x04, 0x07, 0x0B, 0x0E, 0x11, 0x13, 0x18},
	  {0x00, 0x01, 0x03, 0x03, 0x05, 0x07, 0x08, 0x0B, 0x0D, 0x0F, 0x13}
};
UINT8 R898_TF_Result_Low[R898_TF_SIZE][R898_TF_LOW_NUM] = 
{  
	  {0x00, 0x01, 0x03, 0x07, 0x0C, 0x11, 0x1B, 0x2F, 0x6D},  //270n/39n
	  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x08},  //Bead/68n
	  {0x00, 0x01, 0x03, 0x07, 0x0C, 0x11, 0x1B, 0x2F, 0x6D},  //270n/27n   
	  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x08},  //Bead/27n
	  {0x00, 0x02, 0x03, 0x03, 0x05, 0x0C, 0x19, 0x26, 0x37},  //390n/27n
	  {0x00, 0x02, 0x03, 0x03, 0x05, 0x0C, 0x19, 0x26, 0x37}   //390n/39n
};


UINT8  R898_TF[MAX_TUNER_NUM] = {0, 0, 0, 0, 0, 0};
//----------------------------------------------------------//
//                   Internal static struct                         //
//----------------------------------------------------------//
static SysFreq_Info_Type  SysFreq_Info1;
static Sys_Info_Type         Sys_Info1;
static Freq_Info_Type       Freq_Info1;
//----------------------------------------------------------//
//                   Internal Functions                            //
//----------------------------------------------------------//


R898_ErrCode R898_InitReg(R898_TUNER_NUM R898_Tuner_Num, R898_Standard_Type R898_Standard);
R898_ErrCode R898_IMR(R898_TUNER_NUM R898_Tuner_Num, UINT8 IMR_MEM, BOOL IM_Flag);
R898_ErrCode R898_PLL(R898_TUNER_NUM R898_Tuner_Num, UINT32 LO_Freq, R898_Standard_Type R898_Standard);
R898_ErrCode R898_MUX(R898_TUNER_NUM R898_Tuner_Num, UINT32 LO_KHz, UINT32 RF_KHz, R898_Standard_Type R898_Standard);
R898_ErrCode R898_IQ(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Pont);
R898_ErrCode R898_IQ_Tree(R898_TUNER_NUM R898_Tuner_Num, UINT8 FixPot, UINT8 FlucPot, UINT8 PotReg, R898_SectType* CompareTree);
R898_ErrCode R898_IQ_Tree5(R898_TUNER_NUM R898_Tuner_Num, UINT8 FixPot, UINT8 FlucPot, UINT8 PotReg, R898_SectType* CompareTree);
R898_ErrCode R898_CompreCor(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* CorArry);
R898_ErrCode R898_CompreStep(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* StepArry, UINT8 Pace);
R898_ErrCode R898_Muti_Read(R898_TUNER_NUM R898_Tuner_Num, UINT8* IMR_Result_Data);
R898_ErrCode R898_Section(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* SectionArry);
R898_ErrCode R898_F_IMR(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Pont);
R898_ErrCode R898_IMR_Cross(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Pont, UINT8* X_Direct);
R898_ErrCode R898_IMR_Iqcap(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Point);   
R898_ErrCode R898_SetTF(R898_TUNER_NUM R898_Tuner_Num, UINT32 u4FreqKHz, UINT8 u1TfType);
R898_ErrCode R898_SetStandard(R898_TUNER_NUM R898_Tuner_Num, R898_Standard_Type RT_Standard);
R898_ErrCode R898_SetFrequency(R898_TUNER_NUM R898_Tuner_Num, R898_Set_Info R898_INFO);
R898_ErrCode R898_Satellite_Setting(R898_TUNER_NUM R898_Tuner_Num, R898_Set_Info R898_INFO);


Sys_Info_Type R898_Sys_Sel(R898_Standard_Type R898_Standard);
Freq_Info_Type R898_Freq_Sel(UINT32 LO_freq, UINT32 RF_freq, R898_Standard_Type R898_Standard);
SysFreq_Info_Type R898_SysFreq_Sel(R898_Standard_Type R898_Standard,UINT32 RF_freq);

UINT8 R898_Filt_Cal_ADC(R898_TUNER_NUM R898_Tuner_Num, UINT32 IF_Freq, UINT8 R898_BW, UINT8 FilCal_Gap);
static UINT16 R898_Lna_Acc_Gain[4][32] = 
{
	{0,	14,	26,	27,	47,	67,	86,	102,	120,	140,	160,	175,	193,	213,	228,	239,	248,	258,	275,	293,	312,	331,	351,	351,	351},
	{0,	13,	25,	24,	50,	74,	95,	115,	132,	150,	168,	179,	192,	214,	232,	245,	256,	269,	282,	295,	308,	322,	351,	351,	351},
	{0,	1,	13,	13,	41,	64,	85,	103,	118,	133,	147,	157,	167,	192,	213,	229,	241,	259,	268,	277,	286,	293,	324,	324,	324},
	{0,	0,	13,	13,	32,	49,	64,	 79,	 92,	106,	118,	130,	144,	169,	190,	206,	218,	236,	248,	260,	272,	282,	309,	309,	309},

};

static INT8 Lna_Acc_Gain_offset[86]={11,	7,	6,	-4,	-4,	-9,	-14,	-11,	-7,	2,	//45~145
									18,	19,	20,	21,	22,	23,	24,	25,	26,	27,	//145~245
									4,	6,	-2,	4,	0,	-14,	-8,	1,	-2,	-6,	//245~345
									-7,	-17,	-5,	-3,	-6,	8,	4,	17,	15,	15,	//345~445
									25,	18,	25,	23,	30,	-11,	-3,	5,	7,	4,	//445~545
									7,	1,	-2,	0,	-13,	-1,	-6,	-3,	-9,	-12,	//545~645
									-21,	-23,	-23,	-20,	-19,	-9,	5,	5,	0,	-9,	//645~745
									-8,	-7,	2,	8,	10,	10,	6,	2,	-4,	-1,	//745~845
									-3	-2	-4	-2	-3	-6};	//845~905				

static UINT16 R898_Rf_Acc_Gain[16] = 
{
 0, 12, 25, 38, 49, 62, 74, 85, 97, 110,			//0~9
121, 137, 148, 161, 170, 177						//10~15
};

static UINT16 R898_Mixer_Acc_Gain[16] = 
{
 0, 13, 26, 38, 51, 63, 76, 88, 100, 111,			//0~9
 121, 122, 122, 122, 122, 122						//10~12
};


static UINT16 Satellite_Lna_Acc_Gain[19] = 
{
 0, 26, 42, 74, 103, 129, 158, 181, 188, 200,  //0~9
 220, 248, 280, 312, 341, 352, 366, 389, 409,	//10~19
};


Sys_Info_Type R898_Sys_Sel(R898_Standard_Type R898_Standard)
{
	Sys_Info_Type R898_Sys_Info;

	switch (R898_Standard)
	{





	case R898_ISDB_T_IF_5M:
		R898_Sys_Info.IF_KHz=5000;  
		R898_Sys_Info.BW=0x00;                  		
		R898_Sys_Info.FILT_CAL_IF=7550;          //CAL IF  
		R898_Sys_Info.HPF_COR_R36_0_4BT=5;	 //R36[3:0]=3
		R898_Sys_Info.LNA_DET_R_R54_2_2BT = 1;       //0: short;1: 300K;2: 500K;3: 945K;
		//R898_Sys_Info.FILT_EXT_WIDEST=0x00;//R38[2]=0, ext normal
		//R898_Sys_Info.FILT_EXT_POINT=0x03;   //R38[1:0]=11, buf 8
		break;

	case R898_ISDB_C_IF_5M:
		R898_Sys_Info.IF_KHz=5000;
		R898_Sys_Info.BW=0x00;                           //BW=8M      R898:R19[6:5]
	
		R898_Sys_Info.FILT_CAL_IF=7900;          //CAL IF   
		R898_Sys_Info.HPF_COR_R36_0_4BT=6;	 //R36[3:0]=3

		R898_Sys_Info.LNA_DET_R_R54_2_2BT = 1;       //0: short;1: 300K;2: 500K;3: 945K;
		break;

	default:  //R898_DVB_T_8M
		R898_Sys_Info.IF_KHz=5000;  
		R898_Sys_Info.BW=0x00;                           //BW=8M    R898:R19[6:5]
		R898_Sys_Info.FILT_CAL_IF=7550;          //CAL IF  
		R898_Sys_Info.HPF_COR_R36_0_4BT=3;	 //R36[3:0]=3
		R898_Sys_Info.LNA_DET_R_R54_2_2BT = 1;       //0: short;1: 300K;2: 500K;3: 945K;
		//R898_Sys_Info.FILT_EXT_WIDEST=0x00;//R38[2]=0, ext normal
		//R898_Sys_Info.FILT_EXT_POINT=0x03;   //R38[1:0]=11, buf 8
		break;




	//Set By DTV/ATV
		//R898_Sys_Info.INDUC_BIAS = 0x01;      //normal   (R4[0]=1)
	}

		//R898_Sys_Info.INDUC_BIAS = 0x01;     //normal      
		//R898_Sys_Info.SWCAP_CLK = 0x01;     //32k       








	//Filter 3dB


	//BW 1.7M

	//TF Type select


			/* if(R898_TF_Check_Result[R898_Tuner_Num]==1)  //fail
			 {
				R898_DetectTfType_Cal = R898_UL_USING_BEAD;
			 }*/


				if(R898_DetectTfType == R898_UL_USING_BEAD)
				{
					if(R898_DetectMidTfType_def==R898_MID_USING_27NH) //Mid=27
						R898_SetTfType_UL_MID = R898_TF_BEAD_27N;		
					else  //Mid=68
						R898_SetTfType_UL_MID = R898_TF_BEAD_68N;	      	   
				}
				else
				{
					R898_SetTfType_UL_MID = R898_TF_270N_39N;   
				}	

				//R898_SetTfType_UL_MID = R898_TF_270N_39N;   


	return R898_Sys_Info;
}


Freq_Info_Type R898_Freq_Sel(UINT32 LO_freq, UINT32 RF_freq, R898_Standard_Type R898_Standard)
{
	Freq_Info_Type R898_Freq_Info;



		//----- RF dependent parameter --------
	//LNA band 
	if(RF_freq<R898_LNA_MID_LOW[R898_SetTfType_UL_MID])  
		 R898_Freq_Info.LNA_BAND = 3;   //R15[1:0]=2'b11; low
	else if((RF_freq>=R898_LNA_MID_LOW[R898_SetTfType_UL_MID]) && (RF_freq<R898_LNA_HIGH_MID[R898_SetTfType_UL_MID]))  //388~612
		 R898_Freq_Info.LNA_BAND = 2;   //R15[1:0]=2'b10; mid
	else     // >612
		 R898_Freq_Info.LNA_BAND = 1;   //R15[1:0]=00 or 01; high 
	
	//----- LO dependent parameter --------
	//IMR point 
	if((LO_freq>=0) && (LO_freq<205000))  
         R898_Freq_Info.IMR_MEM = 0;   
	else if((LO_freq>=133000) && (LO_freq<400000))  
         R898_Freq_Info.IMR_MEM = 1;   
	else if((LO_freq>=221000) && (LO_freq<760000))  
		 R898_Freq_Info.IMR_MEM = 2;  
	/*else if((LO_freq>=450000) && (LO_freq<775000))  
		 R898_Freq_Info.IMR_MEM = 3; */
	else 
		 R898_Freq_Info.IMR_MEM = 3; 

	//RF polyfilter band   R33[7:6]
	if((LO_freq>=0) && (LO_freq<133000))  
         R898_Freq_Info.RF_POLY = 2;   //low	
	else if((LO_freq>=133000) && (LO_freq<205000))  
         R898_Freq_Info.RF_POLY = 1;   // mid
	else if((LO_freq>=221000) && (LO_freq<760000))  
		 R898_Freq_Info.RF_POLY = 0;   // highest
	else
		 R898_Freq_Info.RF_POLY = 0;   // ultra high

	
	//LPF Cap, Notch



			if((LO_freq>=0) && (LO_freq<73000))  
			{
				R898_Freq_Info.LPF_CAP = 8;
				R898_Freq_Info.LPF_NOTCH = 10;
			}
			else if((LO_freq>=73000) && (LO_freq<81000))
			{
				R898_Freq_Info.LPF_CAP = 8;
				R898_Freq_Info.LPF_NOTCH = 4;
			}
			else if((LO_freq>=81000) && (LO_freq<89000))
			{
				R898_Freq_Info.LPF_CAP = 8;
				R898_Freq_Info.LPF_NOTCH = 3;
			}
			else if((LO_freq>=89000) && (LO_freq<121000))
			{
				R898_Freq_Info.LPF_CAP = 6;
				R898_Freq_Info.LPF_NOTCH = 1;
			}
			else if((LO_freq>=121000) && (LO_freq<145000))
			{
				R898_Freq_Info.LPF_CAP = 4;
				R898_Freq_Info.LPF_NOTCH = 0;
			}
			else if((LO_freq>=145000) && (LO_freq<153000))
			{
				R898_Freq_Info.LPF_CAP = 3;
				R898_Freq_Info.LPF_NOTCH = 0;
			}
			else if((LO_freq>=153000) && (LO_freq<177000))
			{
				R898_Freq_Info.LPF_CAP = 2;
				R898_Freq_Info.LPF_NOTCH = 0;
			}
			else if((LO_freq>=177000) && (LO_freq<201000))
			{
				R898_Freq_Info.LPF_CAP = 1;
				R898_Freq_Info.LPF_NOTCH = 0;
			}
			else //LO>=201M
			{
				R898_Freq_Info.LPF_CAP = 0;
				R898_Freq_Info.LPF_NOTCH = 0;
			}





			 if(LO_freq<=245000)  
 	             R898_Freq_Info.BYP_LPF = 1;      //low pass  (R21[7]=1)
			 else
				 R898_Freq_Info.BYP_LPF = 0;      //bypass  (R21[7]=0)



	return R898_Freq_Info;

}



SysFreq_Info_Type R898_SysFreq_Sel(R898_Standard_Type R898_Standard,UINT32 RF_freq)
{
	SysFreq_Info_Type R898_SysFreq_Info;

	switch(R898_Standard)
	{
	

	case R898_ISDB_T_IF_5M:	

			R898_SysFreq_Info.QCTRL_29_R21_3_1BT = 0;     //0: normal, 1: only 29Q
			R898_SysFreq_Info.LNA_TF_QCTRL_R21_2_1BT = 0;      //0: off, 1: 1.5k
			R898_SysFreq_Info.LNA_MAX_GAIN_R20_0_2BT = 3;  //0: 29, 1: 30, 2: X, 3: 31
			R898_SysFreq_Info.LNA_MIN_GAIN_R21_0_1BT = 1;  //0: +3, 1: 0;
			R898_SysFreq_Info.LNA_TOP_R51_0_3BT = 3;       
			R898_SysFreq_Info.EXTRA_LNA_TOP_R51_3_1BT = 0; //0:high,1:low
			R898_SysFreq_Info.LNA_VTH_R19_3_4BT = R898_VTH_1_27;
			R898_SysFreq_Info.LNA_VTL_R48_4_4BT = R898_VTL_0_80;
			R898_SysFreq_Info.RF_TOP_R51_4_3BT = 0;
			R898_SysFreq_Info.RF_VTH_R24_1_4BT = R898_VTH_0_94;
			R898_SysFreq_Info.RF_VTL_R49_4_4BT = R898_VTL_0_58;
			R898_SysFreq_Info.NRB_TOP_R52_0_4BT = 6;
			R898_SysFreq_Info.NRB_BW_LPF_R52_4_2BT = 2;//0: widest, 1: wide, 2: low, 3: lowest
			R898_SysFreq_Info.NRB_DET_HPF_R38_2_2BT = 3; //0:lowest corner,1: low corner,2:high corner,3:highest corner
			R898_SysFreq_Info.MIXER_TOP_R54_1_3BT = 5;  //0~7
			R898_SysFreq_Info.MIXER_VTH_R25_4_4BT = R898_VTH_1_27;
			R898_SysFreq_Info.MIXER_VTL_R50_4_4BT = R898_VTL_0_80;
			R898_SysFreq_Info.FILTER_TOP_R29_6_2BT = 3; //0: lowest;3: highest
			R898_SysFreq_Info.FILTER_VTH_R36_4_4BT = R898_VTH_1_27;
			R898_SysFreq_Info.FILTER_VTL_R38_4_4BT = R898_VTL_0_69;
			R898_SysFreq_Info.MIXG_LIMIT_R48_0_3BT = 3;//0: max=6,1: max=7,2: max=8,3: max=9,4: max=10,5: max=11,6: max=12,7: max=max gain
			R898_SysFreq_Info.LIMITG_IMG_ADD_R27_4_2BT = 0;
			R898_SysFreq_Info.MIXER_AMP_LPF_R26_5_3BT = 1;  //0: normal (widest);7: narrowest
			R898_SysFreq_Info.MIXER_LPF_ADD_R44_2_2BT = 2;  //0: normal (widest),1:+2,2:+4,3:+6
			R898_SysFreq_Info.RF_BUF_LPF_R49_0_2BT = 3 ;//0: lowest corner; 3: highest corner;
			R898_SysFreq_Info.RBG_MIN_RFBUF_R25_0_1BT = 0;  //0: 0, 1: 1
			R898_SysFreq_Info.RBG_MAX_RFBUF_R24_7_1BT = 15; //0: 12, 1: 15
			R898_SysFreq_Info.LNA_DIS_MODE_R53_5_1BT = 1;   //0: man slow ,1: man slow+fast
			R898_SysFreq_Info.RF_DIS_MODE_R53_6_1BT = 0;    //0: man slow ,1: man slow+fast
			R898_SysFreq_Info.LNA_DIS_CURR_R54_0_1BT = 0; //0: 1/3, 1: normal
			R898_SysFreq_Info.RF_DIS_CURR_R53_0_1BT = 0;  //0: 1/3, 1: normal
			R898_SysFreq_Info.LNA_RF_DIS_SLOW_R55_0_2BT = 0;  //0: 0.15uA, 1:0.3uA, 2: 0.45uA, 3: 0.6uA
			R898_SysFreq_Info.LNA_RF_DIS_FAST_R55_2_2BT = 1;  //0: 0.9uA, 1: 1.5uA, 2: 2.4uA, 3: 3uA
			R898_SysFreq_Info.SLOW_DIS_ENABLE_R56_1_1BT = 0;  //0:disable, 1:enable
			R898_SysFreq_Info.BB_DIS_CURR_R55_6_1BT = 1;     //0: /1, 1: /2
			R898_SysFreq_Info.MIXER_FILTER_DIS_R55_4_2BT = 1; //0: highest, 1: high, 2: low, 3: lowest
			R898_SysFreq_Info.FAGC_2_SAGC_R56_4_1BT = 0;      //0:slow; 1:fast
			R898_SysFreq_Info.CLK_FAST_R58_6_2BT = 3;//0: 250; 1: 500; 2: 1K; 3: 2K
			R898_SysFreq_Info.CLK_SLOW_R58_4_2BT = 3; //0: 1, 1: 4, 2: 16, 3: 64
			R898_SysFreq_Info.LEVEL_SW_R57_4_1BT = 1; //0: disable; 1: enable
			R898_SysFreq_Info.ATV_MODE_SEL_R57_7_1BT = 0;//0: terrestial mode, 1: Cable mode
			R898_SysFreq_Info.LEVEL_SW_VTHH_R57_5_1BT = 0; //0: 1.84V, 1: 1.94V
			R898_SysFreq_Info.LEVEL_SW_VTLL_R57_6_1BT = 0; //0: 0.34V, 1: 0.43V
			//R898_SysFreq_Info.ACI_WIDEN_VTH_R47_2_1BT = 0; //0: from mix, 1: from img
			R898_SysFreq_Info.FILTER_G_CONTROL_R47_1_1BT = 1;//0:ctrl by filtg (>9=0, <6=1), 1:diable
			R898_SysFreq_Info.RF_WIDEN_VTH_VTL_R31_6_2BT = 1;//0: debug mode(no change),1: small, 2: mId, 3: biggest
			R898_SysFreq_Info.BB_WIDEN_VTH_VTL_R47_4_2BT = 0;//0: debug mode(no change),1: small, 2: mId, 3: biggest
			R898_SysFreq_Info.IMG_NRB_ADDER_R17_2_2BT = 3; //0: normal, 1: top+6, 2: top+9, 3: top+11
			R898_SysFreq_Info.LPF_3TH_HGAIN_R28_7_1BT = 0;//0:normal,1:+17dB
			R898_SysFreq_Info.LPF_3TH_GAIN_R37_6_2BT = 1; //0: normal,1: 1.5dB, 2: 3.5dB, 3: 4.5dB		
			R898_SysFreq_Info.VGA_INR_R26_4_1BT = 1; //0: 2K, 1: 16K
			R898_SysFreq_Info.VGA_INC_R47_5_2BT = 3;//0: 0dB, 1: 1.5dB, 2: 3dB, 3: 4dB
			R898_SysFreq_Info.ATTUN_VGA_R31_3_1BT = 1; //0: +4dB, 1: normal
			R898_SysFreq_Info.VGA_SHIFTGAIN_R31_2_1BT = 0; //0: normal, 1: +3dB;
			R898_SysFreq_Info.VGA_ATT5DB_R31_4_1BT = 1; //0: normal, 1: -5dB;

			

			if(RF_freq<=235000)
			{
				R898_SysFreq_Info.LNA_TF_QCTRL_R21_2_1BT = 1;      //0: off, 1: 1.5k
				R898_SysFreq_Info.LNA_MAX_GAIN_R20_0_2BT = 1;  //0: 29, 1: 30, 2: X, 3: 31
				//R898_SysFreq_Info.MIXER_TOP_R54_1_3BT = 5;  //0~7
				R898_SysFreq_Info.NRB_BW_LPF_R52_4_2BT = 3;//0: widest, 1: wide, 2: low, 3: lowest
				//R898_SysFreq_Info.MIXG_LIMIT_R48_0_3BT = 5;//0: max=6,1: max=7,2: max=8,3: max=9,4: max=10,5: max=11,6: max=12,7: max=max gain
			}
			else
			{
				
			}

		break;
		case R898_ISDB_C_IF_5M:	
			R898_SysFreq_Info.QCTRL_29_R21_3_1BT = 0;     //0: normal, 1: only 29Q
			R898_SysFreq_Info.LNA_TF_QCTRL_R21_2_1BT = 0;      //0: off, 1: 1.5k
			R898_SysFreq_Info.LNA_MAX_GAIN_R20_0_2BT = 3;  //0: 29, 1: 30, 2: X, 3: 31
			R898_SysFreq_Info.LNA_MIN_GAIN_R21_0_1BT = 1;  //0: +3, 1: 0;
			R898_SysFreq_Info.LNA_TOP_R51_0_3BT = 1;       
			R898_SysFreq_Info.EXTRA_LNA_TOP_R51_3_1BT = 0; //0:high,1:low
			R898_SysFreq_Info.LNA_VTH_R19_3_4BT = R898_VTH_1_27;
			R898_SysFreq_Info.LNA_VTL_R48_4_4BT = R898_VTL_0_80;
			R898_SysFreq_Info.RF_TOP_R51_4_3BT = 0;
			R898_SysFreq_Info.RF_VTH_R24_1_4BT = R898_VTH_0_94;
			R898_SysFreq_Info.RF_VTL_R49_4_4BT = R898_VTL_0_58;
			R898_SysFreq_Info.NRB_TOP_R52_0_4BT = 10;
			R898_SysFreq_Info.NRB_BW_LPF_R52_4_2BT = 2;//0: widest, 1: wide, 2: low, 3: lowest
			R898_SysFreq_Info.NRB_DET_HPF_R38_2_2BT = 3; //0:lowest corner,1: low corner,2:high corner,3:highest corner
			R898_SysFreq_Info.MIXER_TOP_R54_1_3BT = 5;  //0~7
			R898_SysFreq_Info.MIXER_VTH_R25_4_4BT = R898_VTH_1_27;
			R898_SysFreq_Info.MIXER_VTL_R50_4_4BT = R898_VTL_0_80;
			R898_SysFreq_Info.FILTER_TOP_R29_6_2BT = 3; //0: lowest;3: highest
			R898_SysFreq_Info.FILTER_VTH_R36_4_4BT = R898_VTH_1_27;
			R898_SysFreq_Info.FILTER_VTL_R38_4_4BT = R898_VTL_0_69;
			R898_SysFreq_Info.MIXG_LIMIT_R48_0_3BT = 5;//0: max=6,1: max=7,2: max=8,3: max=9,4: max=10,5: max=11,6: max=12,7: max=max gain
			R898_SysFreq_Info.LIMITG_IMG_ADD_R27_4_2BT = 3;
			R898_SysFreq_Info.MIXER_AMP_LPF_R26_5_3BT = 1;  //0: normal (widest);7: narrowest
			R898_SysFreq_Info.MIXER_LPF_ADD_R44_2_2BT = 0;  //0: normal (widest),1:+2,2:+4,3:+6
			R898_SysFreq_Info.RF_BUF_LPF_R49_0_2BT = 3 ;//0: lowest corner; 3: highest corner;
			R898_SysFreq_Info.RBG_MIN_RFBUF_R25_0_1BT = 0;  //0: 0, 1: 1
			R898_SysFreq_Info.RBG_MAX_RFBUF_R24_7_1BT = 15; //0: 12, 1: 15
			R898_SysFreq_Info.LNA_DIS_MODE_R53_5_1BT = 1;   //0: man slow ,1: man slow+fast
			R898_SysFreq_Info.RF_DIS_MODE_R53_6_1BT = 0;    //0: man slow ,1: man slow+fast
			R898_SysFreq_Info.LNA_DIS_CURR_R54_0_1BT = 0; //0: 1/3, 1: normal
			R898_SysFreq_Info.RF_DIS_CURR_R53_0_1BT = 0;  //0: 1/3, 1: normal
			R898_SysFreq_Info.LNA_RF_DIS_SLOW_R55_0_2BT = 0;  //0: 0.15uA, 1:0.3uA, 2: 0.45uA, 3: 0.6uA
			R898_SysFreq_Info.LNA_RF_DIS_FAST_R55_2_2BT = 1;  //0: 0.9uA, 1: 1.5uA, 2: 2.4uA, 3: 3uA
			R898_SysFreq_Info.SLOW_DIS_ENABLE_R56_1_1BT = 0;  //0:disable, 1:enable
			R898_SysFreq_Info.BB_DIS_CURR_R55_6_1BT = 1;     //0: /1, 1: /2
			R898_SysFreq_Info.MIXER_FILTER_DIS_R55_4_2BT = 1; //0: highest, 1: high, 2: low, 3: lowest
			R898_SysFreq_Info.FAGC_2_SAGC_R56_4_1BT = 0;      //0:slow; 1:fast
			R898_SysFreq_Info.CLK_FAST_R58_6_2BT = 3;//0: 250; 1: 500; 2: 1K; 3: 2K
			R898_SysFreq_Info.CLK_SLOW_R58_4_2BT = 3; //0: 1, 1: 4, 2: 16, 3: 64
			R898_SysFreq_Info.LEVEL_SW_R57_4_1BT = 1; //0: disable; 1: enable
			R898_SysFreq_Info.ATV_MODE_SEL_R57_7_1BT = 0;//0: terrestial mode, 1: Cable mode
			R898_SysFreq_Info.LEVEL_SW_VTHH_R57_5_1BT = 0; //0: 1.84V, 1: 1.94V
			R898_SysFreq_Info.LEVEL_SW_VTLL_R57_6_1BT = 0; //0: 0.34V, 1: 0.43V
			R898_SysFreq_Info.FILTER_G_CONTROL_R47_1_1BT = 1;//0:ctrl by filtg (>9=0, <6=1), 1:diable
			R898_SysFreq_Info.RF_WIDEN_VTH_VTL_R31_6_2BT = 1;//0: debug mode(no change),1: small, 2: mId, 3: biggest
			R898_SysFreq_Info.BB_WIDEN_VTH_VTL_R47_4_2BT = 0;//0: debug mode(no change),1: small, 2: mId, 3: biggest
			R898_SysFreq_Info.IMG_NRB_ADDER_R17_2_2BT = 3; //0: normal, 1: top+6, 2: top+9, 3: top+11
			R898_SysFreq_Info.LPF_3TH_HGAIN_R28_7_1BT = 0;//0:normal,1:+17dB
			R898_SysFreq_Info.LPF_3TH_GAIN_R37_6_2BT = 1; //0: normal,1: 1.5dB, 2: 3.5dB, 3: 4.5dB		
			R898_SysFreq_Info.VGA_INR_R26_4_1BT = 1; //0: 2K, 1: 16K
			R898_SysFreq_Info.VGA_INC_R47_5_2BT = 3;//0: 0dB, 1: 1.5dB, 2: 3dB, 3: 4dB
			R898_SysFreq_Info.ATTUN_VGA_R31_3_1BT = 1; //0: +4dB, 1: normal
			R898_SysFreq_Info.VGA_SHIFTGAIN_R31_2_1BT = 0; //0: normal, 1: +3dB;
			R898_SysFreq_Info.VGA_ATT5DB_R31_4_1BT = 1; //0: normal, 1: -5dB;
			if(RF_freq<=235000)
			{
				R898_SysFreq_Info.LNA_TF_QCTRL_R21_2_1BT = 1;      //0: off, 1: 1.5k
				R898_SysFreq_Info.LNA_MAX_GAIN_R20_0_2BT = 1;  //0: 29, 1: 30, 2: X, 3: 31
				R898_SysFreq_Info.NRB_BW_LPF_R52_4_2BT = 3;//0: widest, 1: wide, 2: low, 3: lowest
			}
			else
			{
			}
		break;

		default:  //other standard


        break;
	}//end switch

	return R898_SysFreq_Info;
	
	}




R898_ErrCode R898_Init(R898_TUNER_NUM R898_Tuner_Num)
{
	UINT8 i;
		
	if(R898_Initial_done_flag[R898_Tuner_Num]==FALSE)
	{
		//reset filter cal.
		for (i=0; i<R898_STD_SIZE; i++)
		{	  
		  R898_Fil_Cal_flag[R898_Tuner_Num][i] = FALSE;
		  R898_Fil_Cal_code[R898_Tuner_Num][i] = 0;
		  R898_Fil_Cal_BW[R898_Tuner_Num][i] = 0x00;
		  R898_Fil_Cal_LpfLsb[R898_Tuner_Num][i] = 0;  //R22[0]
		}

		/*for (i=0; i<5; i++)
	    {	
			R898_IMR_Data[R898_Tuner_Num][i].Gain_X = 0;
			R898_IMR_Data[R898_Tuner_Num][i].Phase_Y = 0;
			R898_IMR_Data[R898_Tuner_Num][i].Iqcap = 0;
			R898_IMR_Data[R898_Tuner_Num][i].Value = 0;
		}*/
		
		R898_IMR_Cal_Result[R898_Tuner_Num] = 0; 
		R898_TF_Check_Result[R898_Tuner_Num] = 0;
		R898_T_SBY_FLAG[R898_Tuner_Num] = 0;

		if(R898_IMR_done_flag[R898_Tuner_Num]==FALSE)
		{

			//if(R898_InitReg(R898_Tuner_Num, R898_STD_SIZE) != RT_Success)        
			 //return RT_Fail;

			//if(R898_TF_Check(R898_Tuner_Num) != RT_Success)        
			 //return RT_Fail;

		  //start IMR calibration
		  if(R898_InitReg(R898_Tuner_Num, R898_STD_SIZE) != RT_Success)        //write initial reg before doing IMR Cal
			 return RT_Fail;

		  //if(R898_Cal_Prepare(R898_Tuner_Num, R898_IMR_CAL) != RT_Success)     
			  //return RT_Fail;

		  if(R898_IMR(R898_Tuner_Num, 2, TRUE) != RT_Success)       //Full K node 2
			return RT_Fail;

		  if(R898_IMR(R898_Tuner_Num, 0, FALSE) != RT_Success)
			return RT_Fail;

		  if(R898_IMR(R898_Tuner_Num, 1, FALSE) != RT_Success)
			return RT_Fail;

		  /*if(R898_IMR(R898_Tuner_Num, 3, FALSE) != RT_Success)
			return RT_Fail;*/

		  if(R898_IMR(R898_Tuner_Num, 3, FALSE) != RT_Success)   
			return RT_Fail;
		  if(R898_IMR(R898_Tuner_Num, 4, FALSE) != RT_Success)   
			return RT_Fail;

		  R898_IMR_done_flag[R898_Tuner_Num] = TRUE;
		}

		R898_Initial_done_flag[R898_Tuner_Num] = TRUE;

	} ///end if(check init flag)

		//write initial reg
		if(R898_InitReg(R898_Tuner_Num, R898_STD_SIZE) != RT_Success)
			return RT_Fail;

		R898_pre_standard[R898_Tuner_Num] = R898_STD_SIZE;


	return RT_Success;
}



R898_ErrCode R898_InitReg(R898_TUNER_NUM R898_Tuner_Num, R898_Standard_Type R898_Standard)
{
	UINT8 InitArrayCunt = 0;
	UINT8 XtalCap, CapTot;
	
	R898_I2C_Len.RegAddr = 0x00;   //  R898_T:0x10
	R898_I2C_Len.Len = R898_T_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];

	
	if(R898_XTAL_CAP>30)
	{
		CapTot = R898_XTAL_CAP-10;
		XtalCap = 1;  //10
	}
	else
	{
		CapTot = R898_XTAL_CAP;
		XtalCap = 0;  //0
	}
	R898_T_iniArray_special[33]=(R898_T_iniArray_special[33] & 0xC0) |  (XtalCap<<5) | CapTot;

	for(InitArrayCunt = 0; InitArrayCunt<R898_T_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_T_iniArray_special[InitArrayCunt];
		R898_T_Array[InitArrayCunt] = R898_T_iniArray_special[InitArrayCunt];
	}

	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;



	//R898_ADDRESS = R898_S_ADDRESS;	//

	R898_I2C_Len.RegAddr = 0x00;   //  R898_S:0x00
	R898_I2C_Len.Len = R898_S_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_SATELITE];

	for(InitArrayCunt = 0; InitArrayCunt<R898_S_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_S_iniArray[InitArrayCunt];
		R898_S_Array[InitArrayCunt] = R898_S_iniArray[InitArrayCunt];
	}



	
	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;

	return RT_Success;
}

R898_ErrCode R898_IMR(R898_TUNER_NUM R898_Tuner_Num, UINT8 IMR_MEM, BOOL IM_Flag)
{


	UINT8  IMR_Gain = 0;
	UINT8  IMR_Phase = 0;
	R898_SectType IMR_POINT;
	UINT8 InitArrayCunt = 0;

	UINT8  LPF_Count = 0;
	UINT8  ADC_Read_Value = 0;
	UINT8 XtalCap, CapTot;

	
	
	R898_I2C_Len.RegAddr = 0x00;   //  R898_T:0x00
	R898_I2C_Len.Len = R898_T_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];


	if(R898_XTAL_CAP>30)
	{
		CapTot = R898_XTAL_CAP-10;
		XtalCap = 1;  //10
	}
	else
	{
		CapTot = R898_XTAL_CAP;
		XtalCap = 0;  //0
	}


	R898_T_IMR_CAL[IMR_MEM][33]=(R898_T_IMR_CAL[IMR_MEM][33] & 0xC0) |  (XtalCap<<5) | CapTot;



	for(InitArrayCunt = 0; InitArrayCunt<R898_T_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_T_IMR_CAL[IMR_MEM][InitArrayCunt];
		R898_T_Array[InitArrayCunt] = R898_T_IMR_CAL[IMR_MEM][InitArrayCunt];
	}

	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;



	R898_I2C_Len.RegAddr = 0x00;   //  R898_S:0x00
	R898_I2C_Len.Len = R898_S_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_SATELITE];

	for(InitArrayCunt = 0; InitArrayCunt<R898_S_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_S_IMR_CAL[IMR_MEM][InitArrayCunt];
		R898_S_Array[InitArrayCunt] = R898_S_IMR_CAL[IMR_MEM][InitArrayCunt];
	}

	
	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 0x00;   //  R898_T:0x00
	R898_I2C.I2cAddr=R898_ADDRESS[R898_TUNER];
	for(LPF_Count=0; LPF_Count < 16; LPF_Count++)  //start from 5
	{
		R898_I2C.RegAddr = 37;	//  R37[0]  
		R898_I2C.Data    = ((R898_T_Array[37] & 0xFE));
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;
		R898_I2C.RegAddr = 36; //R36[7:4]  
		R898_I2C.Data    = ((R898_T_Array[36] & 0x0F) | (LPF_Count<<4));  
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;
		R898_Delay_MS(R898_Tuner_Num, FILTER_DELAY);//2
		if(R898_Muti_Read(R898_Tuner_Num, &ADC_Read_Value) != RT_Success)
			return RT_Fail;
		if(ADC_Read_Value > 40*ADC_READ_COUNT)
			break;
		if(LPF_Count==15 && ADC_Read_Value < 40*ADC_READ_COUNT)
		{
			R898_I2C.RegAddr = 31;	//  R36[6]  
			R898_I2C.Data    = (R898_T_Array[31] | 0x04) ;  
			if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
				return RT_Fail;
			for(LPF_Count=12; LPF_Count < 16; LPF_Count ++)
			{
				R898_I2C.RegAddr = 36;	//  R36[7:4]  
				R898_I2C.Data    = (R898_T_Array[36] & 0x0F) + (LPF_Count<<4);  
				if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
					return RT_Fail;
				R898_Delay_MS(R898_Tuner_Num, VGA_DELAY); //
				if(R898_Muti_Read(R898_Tuner_Num, &ADC_Read_Value) != RT_Success)
					return RT_Fail;
				if(ADC_Read_Value > 40*ADC_READ_COUNT)
					break;
			 }
		}
	}
	//clear IQ_cap
	IMR_POINT.Iqcap = R898_T_Array[27] & 0xFC; // R27[1:0] 

	if(IM_Flag == TRUE)
	{
	     if(R898_IQ(R898_Tuner_Num, &IMR_POINT) != RT_Success)
		    return RT_Fail;
	}
	else
	{
		IMR_POINT.Gain_X = R898_IMR_Data[R898_Tuner_Num][2].Gain_X;
		IMR_POINT.Phase_Y = R898_IMR_Data[R898_Tuner_Num][2].Phase_Y;
		IMR_POINT.Value = R898_IMR_Data[R898_Tuner_Num][2].Value;
		if(R898_F_IMR(R898_Tuner_Num, &IMR_POINT) != RT_Success)
			return RT_Fail;
	}
	switch(IMR_MEM)
	{
	case 0:
		R898_IMR_Data[R898_Tuner_Num][0].Gain_X  = IMR_POINT.Gain_X;
		R898_IMR_Data[R898_Tuner_Num][0].Phase_Y = IMR_POINT.Phase_Y;
		R898_IMR_Data[R898_Tuner_Num][0].Value = IMR_POINT.Value;
		R898_IMR_Data[R898_Tuner_Num][0].Iqcap = IMR_POINT.Iqcap;		
		break;
	case 1:
		R898_IMR_Data[R898_Tuner_Num][1].Gain_X  = IMR_POINT.Gain_X;
		R898_IMR_Data[R898_Tuner_Num][1].Phase_Y = IMR_POINT.Phase_Y;
		R898_IMR_Data[R898_Tuner_Num][1].Value = IMR_POINT.Value;
		R898_IMR_Data[R898_Tuner_Num][1].Iqcap = IMR_POINT.Iqcap;
		break;
	case 2:
		R898_IMR_Data[R898_Tuner_Num][2].Gain_X  = IMR_POINT.Gain_X;
		R898_IMR_Data[R898_Tuner_Num][2].Phase_Y = IMR_POINT.Phase_Y;
		R898_IMR_Data[R898_Tuner_Num][2].Value = IMR_POINT.Value;
		R898_IMR_Data[R898_Tuner_Num][2].Iqcap = IMR_POINT.Iqcap;
		break;
	case 3:
		R898_IMR_Data[R898_Tuner_Num][3].Gain_X  = IMR_POINT.Gain_X;
		R898_IMR_Data[R898_Tuner_Num][3].Phase_Y = IMR_POINT.Phase_Y;
		R898_IMR_Data[R898_Tuner_Num][3].Value = IMR_POINT.Value;
		R898_IMR_Data[R898_Tuner_Num][3].Iqcap = IMR_POINT.Iqcap;
		break;
	case 4:
		R898_IMR_Data[R898_Tuner_Num][4].Gain_X  = IMR_POINT.Gain_X;
		R898_IMR_Data[R898_Tuner_Num][4].Phase_Y = IMR_POINT.Phase_Y;
		R898_IMR_Data[R898_Tuner_Num][4].Value = IMR_POINT.Value;
		R898_IMR_Data[R898_Tuner_Num][4].Iqcap = IMR_POINT.Iqcap;
		break;
    default:
		R898_IMR_Data[R898_Tuner_Num][3].Gain_X  = IMR_POINT.Gain_X;
		R898_IMR_Data[R898_Tuner_Num][3].Phase_Y = IMR_POINT.Phase_Y;
		R898_IMR_Data[R898_Tuner_Num][3].Value = IMR_POINT.Value;
		R898_IMR_Data[R898_Tuner_Num][3].Iqcap = IMR_POINT.Iqcap;
		break;
	}
	IMR_Gain = R898_IMR_Data[R898_Tuner_Num][IMR_MEM].Gain_X & 0x3F;
	IMR_Phase = R898_IMR_Data[R898_Tuner_Num][IMR_MEM].Phase_Y & 0x3F;
	if((((IMR_Gain & 0x1E)>>1)>10) || (((IMR_Phase & 0x1E)>>1)>10))
	{
		R898_IMR_Cal_Result[R898_Tuner_Num] = 1; //fail
	}
	return RT_Success;
}


R898_ErrCode R898_PLL(R898_TUNER_NUM R898_Tuner_Num, UINT32 LO_Freq, R898_Standard_Type R898_Standard)
{
	UINT8 InitArrayCunt = 0;
	UINT8  MixDiv = 2;
	UINT8  DivBuf = 0;
	UINT8  Ni = 0;
	UINT8  Si = 0;
	UINT8  DivNum = 0;
	UINT16 Nint = 0;
	UINT32 VCO_Min = 6600000;
	UINT32 VCO_Max = VCO_Min*1.5;
	UINT32 VCO_Freq = 0;
	UINT32 PLL_Ref	= R898_Xtal;
	UINT32 VCO_Fra	= 0;
	UINT32 Nsdm = 2;
	UINT16 SDM = 0;
	UINT16 SDM16to9 = 0;
	UINT16 SDM8to1 = 0;

	UINT16  u2XalDivJudge;




	//UINT8   PW_CLK_AGC = 0x00;
	UINT8 pw0_xtal = 0; //R32[3:0]
	UINT8 gmx2 = 0; //R32[4]
	UINT8 xo_dampR = 0; //R32[6:5]
	UINT8 clkpll_vddres = 0; //R32[7]
	//UINT8   XTAL_AVDD_1 = 0x00;
	//UINT8   RES_HALF = 0x00;
	//UINT8   EN_XTAL_FILT = 0x00;

	UINT8 clkout_R = 0; //R33[6]
	UINT8 pw0_xbuf_ldo = 0; //R34[3]
	UINT8 TC_Xtal = 0; //R34[7]
	UINT8 vref_type = 0; //R39[2]
	UINT8 dead_sw = 0; //R39[3]
	UINT8 clkout_path = 0; //R44[7]
	UINT8 agc_clk_src = 0; //R45[1]
	UINT8 pwd_clkplls = 0; //R45[6]

	UINT8 pw0_clkpll = 0; //R46[3:2]
	UINT8 xbuf_vddR = 0; //R46[4]
	UINT8 xbuf_gm = 0; //R46[5]
	UINT8 xtal_gm_ldo_sel = 0; //R50[3]
	UINT8 LDO_micap = 0; //R51[7]
	UINT8 att_sdm_p = 0; //R63[2:1]
	UINT8 att_sdm_n = 0; //R63[4:3]
	UINT8 pfdcp_ldo = 0; //R63[6:5]
	UINT8 xtal_buf_ldo_sel = 0; //R63[7]
	UINT32 div8,div12,div16,div24,div32,div48,div64,div96,div128,div256=0;
	UINT8 mod23,seldiv,divopt_before,divopt = 0;
	UINT8 seldiv_temp =0;
	UINT8   Spur_24mhz_1_op[42] = {             
	0x1,	0x1,	0x1,	0x1,	0xB,	0x89,	0xC,	0x9,	0x9,	0xC,
	0x89,	0xA,	0x89,	0x9,	0x8C,	0x9,	0x9,	0x89,	0x9,	0xB,
	0xC,	0xC,	0x89,	0xA,	0xA,	0xA,	0x9,	0x9,	0xB,	0xB,
	0xB,	0xC,	0xA,	0x8B,	0x89,	0x9,	0x9,	0x9,	0x9,	0x9,
	0x9,	0x9,																					
	};
	UINT8   Spur_24mhz_2_op[42] = {             
	0x1,	0x1,	0x1,	0x1,	0x23,	0xB7,	0x87,	0x67,	0x47,	0xF7,
	0xE3,	0xB5,	0xB3,	0xE7,	0x75,	0xA5,	0xD5,	0x23,	0x73,	0x23,
	0xA3,	0x27,	0xA3,	0x23,	0xE1,	0x21,	0x71,	0x61,	0x63,	0x83,
	0x71,	0x31,	0x23,	0x3,	0x23,	0x23,	0x31,	0xA3,	0x31,	0x33,
	0x23,	0xE1,													
	};
	UINT8   Spur_24mhz_3_op[42] = {             
	0x1,	0x1,	0x1,	0x1,	0x1C,	0x3B,	0x3C,	0x28,	0x38,	0x3B,
	0x38,	0x39,	0x3F,	0x30,	0x20,	0x32,	0x34,	0x37,	0x37,	0x3B,
	0x37,	0x38,	0x37,	0x38,	0x17,	0x28,	0x37,	0x38,	0x37,	0x10,
	0x3B,	0x18,	0x37,	0x33,	0x1B,	0x3B,	0x37,	0x3B,	0x37,	0x3B,
	0x37,	0x38,																				
	};
	UINT8   Spur_24mhz_4_op[42] = {             
	0x1,	0x1,	0x1,	0x1,	0x50,	0x58,	0x58,	0x10,	0x60,	0x70,
	0x52,	0x52,	0x50,	0x50,	0x54,	0x50,	0x21,	0x50,	0x50,	0x50,
	0x52,	0x50,	0x5A,	0x30,	0x4A,	0x10,	0x42,	0x50,	0x50,	0x50,
	0x22,	0x70,	0x50,	0x50,	0x0,	0x50,	0x50,	0x50,	0x50,	0x50,
	0x50,	0x78,														
	};
	 R898_I2C.RegAddr = 56;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] | 0x10) ;	
	 R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
	 if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;
	 R898_I2C.RegAddr = 47;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x7F) ;	
	 R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
	 if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;



	//Divider
	while(MixDiv <= 64)
	{
		if(((LO_Freq * MixDiv) >= VCO_Min) && ((LO_Freq * MixDiv) < VCO_Max))
		{
			DivBuf = MixDiv;
			while(DivBuf > 2)
			{
				DivBuf = DivBuf >> 1;
				DivNum ++;
			}
			break;
		}
		MixDiv = MixDiv << 1;
	}

    u2XalDivJudge = LO_Freq/24000;

	//DEAD_SW R39[3] 0=0.5ns 1=1.5ns
	//R45[6]
	//// R898_T_Array[45] = (R898_T_Array[45] & 0xBF) | (PW_CLK_AGC<<6);

	//R46[7:3]
	// R898_T_Array[46] = (R898_T_Array[46] & 0x37) | (EN_XTAL_FILT<<3) | (XTAL_OUTPUT<<6);

	 //R62[7] R63[0]
	// R898_T_Array[62] = (R898_T_Array[62] & 0x7F) | (XTAL_AVDD_0<<7);
	// R898_T_Array[63] = (R898_T_Array[63] & 0xFE) | (XTAL_AVDD_1);


	
	pw0_xtal = (Spur_24mhz_1_op[u2XalDivJudge] & 0x0F);//R32[3:0]
	gmx2 = (Spur_24mhz_1_op[u2XalDivJudge] & 0x10) >> 4; //R32[4]
	xo_dampR = (Spur_24mhz_1_op[u2XalDivJudge] & 0x60) >> 5; //R32[6:5]
	clkpll_vddres = (Spur_24mhz_1_op[u2XalDivJudge] & 0x80) >> 7; //R32[7]
	clkout_R = (Spur_24mhz_2_op[u2XalDivJudge] & 0x01); //R33[6]
	pw0_xbuf_ldo = (Spur_24mhz_2_op[u2XalDivJudge] & 0x02) >> 1; //R34[3]
	TC_Xtal = (Spur_24mhz_2_op[u2XalDivJudge] & 0x04) >> 2; //R34[7]
	vref_type = (Spur_24mhz_2_op[u2XalDivJudge] & 0x08) >> 3; //R39[2]
	dead_sw = (Spur_24mhz_2_op[u2XalDivJudge] & 0x10) >> 4; //R39[3]
	clkout_path = (Spur_24mhz_2_op[u2XalDivJudge] & 0x20) >> 5; //R44[7]
	agc_clk_src = (Spur_24mhz_2_op[u2XalDivJudge] & 0x40) >> 6; //R45[1]
	pwd_clkplls = (Spur_24mhz_2_op[u2XalDivJudge] & 0x80) >> 7; //R45[6]
	pw0_clkpll = (Spur_24mhz_3_op[u2XalDivJudge] & 0x03); //R46[3:2]
	xbuf_vddR =  (Spur_24mhz_3_op[u2XalDivJudge] & 0x04)>>2; //R46[4]
	xbuf_gm = (Spur_24mhz_3_op[u2XalDivJudge] & 0x08)>>3; //R46[5]
	xtal_gm_ldo_sel = (Spur_24mhz_3_op[u2XalDivJudge] & 0x10)>>4; //R50[3]
	LDO_micap = (Spur_24mhz_3_op[u2XalDivJudge] & 0x20)>>5; //R51[7]
	att_sdm_p = (Spur_24mhz_4_op[u2XalDivJudge] & 0x03) ;//R63[2:1]
	att_sdm_n = (Spur_24mhz_4_op[u2XalDivJudge] & 0x0C)>>2 ;//R63[4:3]
	pfdcp_ldo =  (Spur_24mhz_4_op[u2XalDivJudge] & 0x30)>>4 ;//R63[6:5]
	xtal_buf_ldo_sel = (Spur_24mhz_4_op[u2XalDivJudge] & 0x40)>>6 ;//R63[7]

	div8 = (LO_Freq)*8;
	div12 = (LO_Freq)*12;
	div16 = (LO_Freq)*16;
	div24 = (LO_Freq)*24;
	div32 = (LO_Freq)*32;
	div48 = (LO_Freq)*48;
	div64 = (LO_Freq)*64;
	div96 = (LO_Freq)*96;
	div128 = (LO_Freq)*128;
	div256 = (LO_Freq)*256;


	 if( (div8 >= VCO_Min) && (div8 < VCO_Max) )  //VCO/8
	 {
            //ComboBoxQuickly(14300).ListIndex = 0
            //ComboBoxQuickly(14301).ListIndex = 0
            mod23 = 0;
            seldiv = 0;
            divopt_before = 2;
            divopt= 8;
	 }
	 else if ((div16 >= VCO_Max) && (div8 < VCO_Min))   //VCO/12
	 {
           //ComboBoxQuickly(14300).ListIndex = 1
           // ComboBoxQuickly(14301).ListIndex = 0
            mod23 = 1;
            seldiv = 0;
            divopt_before = 2;
            divopt= 12;
	 }
	 else if ((div16 >= VCO_Min) && (div16 < VCO_Max))   //VCO/16
	 {
            //ComboBoxQuickly(14300).ListIndex = 0
            //ComboBoxQuickly(14301).ListIndex = 1
            mod23 = 0;
            seldiv = 1;
            divopt_before = 4;
            divopt= 16;
	 }
	 else if ((div32 >= VCO_Max) && (div16 < VCO_Min)) //VCO/24
	 {
            //ComboBoxQuickly(14300).ListIndex = 1
            //ComboBoxQuickly(14301).ListIndex = 1
            mod23 = 1;
            seldiv = 1;
            divopt_before = 4;
            divopt= 24;
	 }
	 else if ((div32 >= VCO_Min) && (div32 < VCO_Max)) //VCO/32
	 {
            //ComboBoxQuickly(14300).ListIndex = 0
            //ComboBoxQuickly(14301).ListIndex = 2
            mod23 = 0;
            seldiv = 2;
            divopt_before = 8;
            divopt= 32;
	 }
	 else if ((div64 >= VCO_Max) && (div32 < VCO_Min))  //VCO/48
	 {
            //ComboBoxQuickly(14300).ListIndex = 1
           // ComboBoxQuickly(14301).ListIndex = 2
            mod23 = 1;
            seldiv = 2;
            divopt_before = 8;
            divopt= 48;
	 }
	 else if ((div64 >= VCO_Min) && (div64 < VCO_Max)) //VCO/64
	 {
            //ComboBoxQuickly(14300).ListIndex = 0
            //ComboBoxQuickly(14301).ListIndex = 3
            mod23 = 0;
            seldiv = 3;
            divopt_before = 16;
            divopt= 64;
	 }
	 else if ((div128 >= VCO_Max) && (div64 < VCO_Min)) //VCO/96
	 {
            //ComboBoxQuickly(14300).ListIndex = 1
            //ComboBoxQuickly(14301).ListIndex = 3
            mod23 = 1;
            seldiv = 3;
            divopt_before = 16;
            divopt= 96;
	 }
	 else if ((div128 >= VCO_Min) && (div128 < VCO_Max)) //VCO/128
	 {
            mod23 = 0;
            seldiv = 4;
            divopt_before = 32;
            divopt= 128;
	 }
	 else  //VCO/196
	 {
            mod23 = 1;
            seldiv = 4;
            divopt_before = 32;
            divopt= 196;
	 }
	 // mod23 R43[0]
	 R898_T_Array[43] =  (R898_T_Array[43] & 0xBF) | (mod23<<6);

	 //div_sel R43[2:1]
	 R898_T_Array[43] =  (R898_T_Array[43] & 0xF8) | (seldiv);

	 if(seldiv == 0)
		seldiv_temp = 2;
	 else if (seldiv==1)
		seldiv_temp = 4;
	 else if (seldiv==2)
		seldiv_temp = 8;
	 else if (seldiv==3)
		seldiv_temp = 16;
	 else if (seldiv==4)
		seldiv_temp = 32;


	 if(TC_Xtal==0)
		PLL_Ref = PLL_Ref;
	 else
		PLL_Ref = PLL_Ref/2;
    
	VCO_Freq =(UINT32) ( (div8 / 8)* 2 * (mod23 + 2) * seldiv_temp);
	//div8 / 8 * 2 * (mod23 + 2) * 2 ^ (seldiv + 1)

	//VCO_Freq = LO_Freq * MixDiv;
	Nint     = (UINT16) (VCO_Freq /( 2 * (mod23 + 2) )/ PLL_Ref);
	//Nint     = (UINT16) (VCO_Freq /(2 )/ PLL_Ref);
	//VCO_Fra  = (UINT16) (VCO_Freq - 2 * (mod23 + 2) * PLL_Ref * Nint);
	VCO_Fra  = (UINT32) (VCO_Freq - (2 * (mod23 + 2) * PLL_Ref * Nint));
	//VCO_Fra  = (UINT16) (VCO_Freq - 2 * PLL_Ref * Nint);

	//Boundary spur prevention
	if (VCO_Fra < (PLL_Ref*(mod23 + 2))/64)           //2*PLL_Ref/128
		VCO_Fra = 0;
	else if (VCO_Fra > (PLL_Ref*(mod23 + 2)*127)/64)  //2*PLL_Ref*127/128
	{
		VCO_Fra = 0;
		Nint ++;
	}
	else if((VCO_Fra > (PLL_Ref*127*(mod23 + 2))/128) && (VCO_Fra < (PLL_Ref*(mod23 + 2)))) //> 2*PLL_Ref*127/256,  < 2*PLL_Ref*128/256
		VCO_Fra = (PLL_Ref*(mod23 + 2)*127)/128;      // VCO_Fra = 2*PLL_Ref*127/256
	else if((VCO_Fra > (PLL_Ref*(mod23 + 2))) && (VCO_Fra < (PLL_Ref*129*(mod23 + 2))/128)) //> 2*PLL_Ref*128/256,  < 2*PLL_Ref*129/256
		VCO_Fra = PLL_Ref*129/128;      // VCO_Fra = 2*PLL_Ref*129/256
	else
		VCO_Fra = VCO_Fra;



	Ni = (UINT8) ((Nint - 13) / 4);
	Si = (UINT8) (Nint - 4 *Ni - 13);

	//Si R40[7:6]
	R898_T_Array[40] = (R898_T_Array[40] & 0x3F) | ((Si << 6));

	//Ni R40[5:0]
	R898_T_Array[40] = (R898_T_Array[40] & 0xC0) | (Ni);


         	
	//pw_sdm		// R45[0]  
	R898_T_Array[45] &= 0xFE;
	if(VCO_Fra == 0)
		R898_T_Array[45] |= 0x01;


	//SDM calculator
	while(VCO_Fra > 1)
	{			
		if (VCO_Fra > (2*(mod23 + 2)*PLL_Ref / Nsdm))
		{		
			SDM = SDM + 32768 / (Nsdm/2);
			VCO_Fra = VCO_Fra - (2*(mod23 + 2)*PLL_Ref) / Nsdm;
			if (Nsdm >= 0x8000)
				break;
		}
		Nsdm = Nsdm << 1;
	}

	//SDM calculator
	/*while(VCO_Fra > 1)
	{			
		if (VCO_Fra > (2*PLL_Ref / Nsdm))
		{		
			SDM = SDM + 32768 / (Nsdm/2);
			VCO_Fra = VCO_Fra - 2*PLL_Ref / Nsdm;
			if (Nsdm >= 0x8000)
				break;
		}
		Nsdm = Nsdm << 1;
	}*/

	SDM16to9 = SDM >> 8;
	SDM8to1 =  SDM - (SDM16to9 << 8);

	//R42[7:0]  
	R898_T_Array[42]    = (UINT8) SDM16to9;
	//R41[7:0] 
	R898_T_Array[41]    = (UINT8) SDM8to1;

	R898_I2C.RegAddr = 32;	  
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x00) | (pw0_xtal) | (gmx2<<4) | (xo_dampR<<5) | (clkpll_vddres<<7);	
	R898_I2C.RegAddr = 33;	 
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xBF) | (clkout_R<<6);
	R898_I2C.RegAddr = 34;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF7) | (pw0_xbuf_ldo<<3) ;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x7F) | (TC_Xtal<<7) ;
	R898_I2C.RegAddr = 39;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF3) | (vref_type<<2) | (dead_sw<<3);
	R898_I2C.RegAddr = 44;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x7F) | (clkout_path<<7);
	R898_I2C.RegAddr = 45;	
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xBD) | (agc_clk_src<<1) | (pwd_clkplls<<6);
	R898_I2C.RegAddr = 46;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xC3) | (pw0_clkpll<<2) | (xbuf_vddR<<4) | (xbuf_gm<<5);
	R898_I2C.RegAddr = 50;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF7) | (xtal_gm_ldo_sel<<3);
	R898_I2C.RegAddr = 51;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x7F) | (LDO_micap<<7);
	R898_I2C.RegAddr = 63;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x01) | (att_sdm_p<<1) | (att_sdm_n<<3) | (pfdcp_ldo<<5) | (xtal_buf_ldo_sel<<7);
	 R898_I2C.RegAddr = 56;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xEF) ;	
	 R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
	 if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;
	 R898_I2C.RegAddr = 47;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] | 0x80) ;	
	 R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
	 if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C_Len.RegAddr = 0x00;   //  R898_T:0x00
	R898_I2C_Len.Len = R898_T_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];
	for(InitArrayCunt = 0; InitArrayCunt<R898_T_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_T_Array[InitArrayCunt];


			//R898_I2C_Len.Data[InitArrayCunt] = R898_T_iniArray[InitArrayCunt];
		//R898_T_Array[InitArrayCunt] = R898_T_iniArray[InitArrayCunt];
	}
	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;

	return RT_Success;

}


R898_ErrCode R898_MUX(R898_TUNER_NUM R898_Tuner_Num, UINT32 LO_KHz, UINT32 RF_KHz, R898_Standard_Type R898_Standard)
{	


	UINT8 Reg08_IMR_Gain   = 0;
	UINT8 Reg09_IMR_Phase  = 0;
	UINT8 Reg03_IMR_Iqcap  = 0;

	R898_I2C.I2cAddr=R898_ADDRESS[R898_TUNER];

	//Freq_Info_Type Freq_Info1;
	Freq_Info1 = R898_Freq_Sel(LO_KHz, RF_KHz, R898_Standard);

	// LNA band (depend on RF_KHz)
	R898_I2C.RegAddr = 20;	// R20[5:4] 
	R898_T_Array[20] = (R898_T_Array[20] & 0xCF) | (Freq_Info1.LNA_BAND<<4);
	R898_I2C.Data = R898_T_Array[20];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	// RF Polyfilter
	R898_I2C.RegAddr = 24;	// R24[6:5]  
	R898_T_Array[24] = (R898_T_Array[24] & 0x9F) |(Freq_Info1.RF_POLY<<5);
	R898_I2C.Data = R898_T_Array[24];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	// LNA Cap
	R898_I2C.RegAddr = 23;	// R23[3:0]  
	R898_T_Array[23] = (R898_T_Array[23] & 0xF0) | (Freq_Info1.LPF_CAP);	
	R898_I2C.Data = R898_T_Array[23];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	// LNA Notch
	R898_I2C.RegAddr = 23;	// R23[7:4] 
	R898_T_Array[23] = (R898_T_Array[23] & 0x0F) | (Freq_Info1.LPF_NOTCH<<4);	
	R898_I2C.Data = R898_T_Array[23];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;
	R898_I2C.RegAddr = 21;	// R21[7] 
	R898_T_Array[21] = (R898_T_Array[21] & 0x7F) | (Freq_Info1.BYP_LPF<<7);	
	R898_I2C.Data = R898_T_Array[21];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	//Set_IMR
	if((R898_IMR_done_flag[R898_Tuner_Num] == TRUE) && (R898_IMR_Cal_Result[R898_Tuner_Num]==0))
	{
		Reg08_IMR_Gain = R898_IMR_Data[R898_Tuner_Num][Freq_Info1.IMR_MEM].Gain_X & 0x3F;
		Reg09_IMR_Phase = R898_IMR_Data[R898_Tuner_Num][Freq_Info1.IMR_MEM].Phase_Y & 0x3F;
		Reg03_IMR_Iqcap = R898_IMR_Data[R898_Tuner_Num][Freq_Info1.IMR_MEM].Iqcap & 0x03;
	}
	else
	{
		Reg08_IMR_Gain = 0;
	    Reg09_IMR_Phase = 0;
		Reg03_IMR_Iqcap = 0;
	}

	R898_I2C.RegAddr = 28; // R28[5:0]            
	R898_T_Array[28] = (R898_T_Array[28] & 0xC0) | Reg08_IMR_Gain;
	R898_I2C.Data = R898_T_Array[28];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 29; // R29[5:0]  
	R898_T_Array[29] = (R898_T_Array[29] & 0xC0) | Reg09_IMR_Phase;
	R898_I2C.Data =R898_T_Array[29]  ;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 27; // R27[1:0]  
	R898_T_Array[27] = (R898_T_Array[27] & 0xFC) | Reg03_IMR_Iqcap;
	R898_I2C.Data =R898_T_Array[27]  ;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	return RT_Success;
}


R898_ErrCode R898_SetTF(R898_TUNER_NUM R898_Tuner_Num, UINT32 u4FreqKHz, UINT8 u1TfType)
{
    UINT8    u1FreqCount = 0;
	UINT32   u4Freq1 = 0;
	UINT32   u4Freq2 = 0;
	UINT32   u4Ratio;
	UINT8    u1TF_Set_Result1 = 0;
	UINT8    u1TF_Set_Result2 = 0;
	UINT8    u1TF_tmp1, u1TF_tmp2;
	UINT16   u2Int;
	
	if((u4FreqKHz>0) && (u4FreqKHz<R898_LNA_MID_LOW[R898_SetTfType_UL_MID]))  //Low
	{

		if(u4FreqKHz<R898_TF_Freq_Low[u1TfType][R898_TF_LOW_NUM-1])
		{
			u1FreqCount = R898_TF_LOW_NUM;
		}
		else
		{
			while((u4FreqKHz < R898_TF_Freq_Low[u1TfType][u1FreqCount]) && (u1FreqCount<R898_TF_LOW_NUM))
			{
			   u1FreqCount++;
			}
		}

		if(u1FreqCount==0)
		{
			R898_TF[R898_Tuner_Num] = R898_TF_Result_Low[u1TfType][0];
		}
		else if(u1FreqCount==R898_TF_LOW_NUM)
        {
			R898_TF[R898_Tuner_Num] = R898_TF_Result_Low[u1TfType][R898_TF_LOW_NUM-1];
		}
		else
		{
			u1TF_Set_Result1 = R898_TF_Result_Low[u1TfType][u1FreqCount-1]; 
			u1TF_Set_Result2 = R898_TF_Result_Low[u1TfType][u1FreqCount]; 
			u4Freq1 = R898_TF_Freq_Low[u1TfType][u1FreqCount-1];
			u4Freq2 = R898_TF_Freq_Low[u1TfType][u1FreqCount];

			u1TF_tmp1 = ((u1TF_Set_Result1 & 0x40)>>2)*3 + (u1TF_Set_Result1 & 0x3F);  //b6 is 3xb4
			u1TF_tmp2 = ((u1TF_Set_Result2 & 0x40)>>2)*3 + (u1TF_Set_Result2 & 0x3F);		
			 
			u4Ratio = (u4Freq1- u4FreqKHz)*100/(u4Freq1 - u4Freq2);

			u2Int = (UINT16)((u1TF_tmp2 - u1TF_tmp1)*u4Ratio/100);
			R898_TF[R898_Tuner_Num] = u1TF_tmp1 + (UINT8)u2Int;
			if(((u1TF_tmp2 - u1TF_tmp1)*u4Ratio - u2Int*100) > 50)			 
			 R898_TF[R898_Tuner_Num] = R898_TF[R898_Tuner_Num] + 1;	

			if(R898_TF[R898_Tuner_Num]>=0x40)
			{
				R898_TF[R898_Tuner_Num] = (R898_TF[R898_Tuner_Num] + 0x10);
			}
		 }
	}
	else if((u4FreqKHz>=R898_LNA_MID_LOW[R898_SetTfType_UL_MID]) && (u4FreqKHz<R898_LNA_HIGH_MID[R898_SetTfType_UL_MID]))  //Mid
    {

		if((u4FreqKHz < R898_TF_Freq_Mid[u1TfType][R898_TF_MID_NUM-1]))
		{
			u1FreqCount = R898_TF_MID_NUM;
		}
		else
		{
			while((u4FreqKHz < R898_TF_Freq_Mid[u1TfType][u1FreqCount]) && (u1FreqCount<R898_TF_MID_NUM))
			{
				u1FreqCount++;
			}
		}

		if(u1FreqCount==0)
		{
			R898_TF[R898_Tuner_Num] = R898_TF_Result_Mid[u1TfType][0];
		}
		else if(u1FreqCount==R898_TF_MID_NUM)
		{
			R898_TF[R898_Tuner_Num] = R898_TF_Result_Mid[u1TfType][R898_TF_MID_NUM-1];
		}
		else
		{
			u1TF_Set_Result1 = R898_TF_Result_Mid[u1TfType][u1FreqCount-1]; 
			u1TF_Set_Result2 = R898_TF_Result_Mid[u1TfType][u1FreqCount]; 
			u4Freq1 = R898_TF_Freq_Mid[u1TfType][u1FreqCount-1];
			u4Freq2 = R898_TF_Freq_Mid[u1TfType][u1FreqCount]; 

			u1TF_tmp1 = ((u1TF_Set_Result1 & 0x40)>>2) + (u1TF_Set_Result1 & 0x3F);  //b6 is 1xb4
			u1TF_tmp2 = ((u1TF_Set_Result2 & 0x40)>>2) + (u1TF_Set_Result2 & 0x3F);	

			u4Ratio = (u4Freq1- u4FreqKHz)*100/(u4Freq1 - u4Freq2);

			u2Int = (UINT16)((u1TF_tmp2 - u1TF_tmp1)*u4Ratio/100);
			R898_TF[R898_Tuner_Num] = u1TF_tmp1 + (UINT8)u2Int;
			if(((u1TF_tmp2 - u1TF_tmp1)*u4Ratio - u2Int*100) > 50)			 
				R898_TF[R898_Tuner_Num] = R898_TF[R898_Tuner_Num] + 1;			 

			if(R898_TF[R898_Tuner_Num]>=0x40)
			{
				R898_TF[R898_Tuner_Num] = (R898_TF[R898_Tuner_Num] + 0x30);
			}
		}
	}
	else  //HIGH
	{
		if(u4FreqKHz < R898_TF_Freq_High[u1TfType][R898_TF_HIGH_NUM-1])
		{
			u1FreqCount = R898_TF_HIGH_NUM;
		}
		else
		{
			while((u4FreqKHz < R898_TF_Freq_High[u1TfType][u1FreqCount]) && (u1FreqCount<R898_TF_HIGH_NUM))
			{
			   u1FreqCount++;
			}
		}
        
		if(u1FreqCount==0)
		{
			R898_TF[R898_Tuner_Num] = R898_TF_Result_High[u1TfType][0];
		}
		else if(u1FreqCount==R898_TF_HIGH_NUM)
        {
			R898_TF[R898_Tuner_Num] = R898_TF_Result_High[u1TfType][R898_TF_HIGH_NUM-1];
		}
		else
		{
			u1TF_Set_Result1 = R898_TF_Result_High[u1TfType][u1FreqCount-1]; 
		    u1TF_Set_Result2 = R898_TF_Result_High[u1TfType][u1FreqCount]; 
		    u4Freq1 = R898_TF_Freq_High[u1TfType][u1FreqCount-1];
		    u4Freq2 = R898_TF_Freq_High[u1TfType][u1FreqCount]; 
			u4Ratio = (u4Freq1- u4FreqKHz)*100/(u4Freq1 - u4Freq2);

			u2Int = (UINT16)((u1TF_Set_Result2 - u1TF_Set_Result1)*u4Ratio/100);
			R898_TF[R898_Tuner_Num] = u1TF_Set_Result1 + (UINT8)u2Int;
			if(((u1TF_Set_Result2 - u1TF_Set_Result1)*u4Ratio - u2Int*100) > 50)			 
			    R898_TF[R898_Tuner_Num] = R898_TF[R898_Tuner_Num] + 1;	             
		}
	}
  
    //Set TF: R22[6:0]
	R898_I2C.RegAddr = 22;
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x80) | R898_TF[R898_Tuner_Num];
	R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr]  ;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

    return RT_Success;
}


R898_ErrCode R898_IQ(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Pont)
{
	R898_SectType Compare_IQ[3];

	UINT8   X_Direction;  // 1:X, 0:Y

	R898_I2C.I2cAddr=R898_ADDRESS[R898_TUNER];
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];
		
	// increase VGA power to let image significant
	/*for(VGA_Count=11; VGA_Count < 16; VGA_Count ++)
	{
		R898_I2C.RegAddr = 36; //R36[7:4]  
		R898_I2C.Data    = ((R898_T_Array[36] & 0x0F) | (VGA_Count<<4));  
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		R898_Delay_MS(R898_Tuner_Num, VGA_DELAY); //
		
		if(R898_Muti_Read(R898_Tuner_Num, &VGA_Read) != RT_Success)
			return RT_Fail;

		if(VGA_Read > 80*ADC_READ_COUNT)
			break;
	}*/

	Compare_IQ[0].Gain_X  = R898_T_Array[28] & 0xC0; // R28[5:0]  
	Compare_IQ[0].Phase_Y = R898_T_Array[29] & 0xC0; // R29[5:0] 


	    // Determine X or Y
	    if(R898_IMR_Cross(R898_Tuner_Num, &Compare_IQ[0], &X_Direction) != RT_Success)
			return RT_Fail;

		if(X_Direction==1)
		{
			//compare and find min of 3 points. determine I/Q direction
		    if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
			  return RT_Fail;

		    //increase step to find min value of this direction
		    if(R898_CompreStep(R898_Tuner_Num, &Compare_IQ[0], 28) != RT_Success)  //X
			  return RT_Fail;	
		}
		else
		{
		   //compare and find min of 3 points. determine I/Q direction
		   if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		   	 return RT_Fail;

		   //increase step to find min value of this direction
		   if(R898_CompreStep(R898_Tuner_Num, &Compare_IQ[0], 29) != RT_Success)  //Y
			 return RT_Fail;	
		}

		//Another direction
		if(X_Direction==1)
		{	    
           //if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 0x10, &Compare_IQ[0]) != RT_Success) //Y	
           if(R898_IQ_Tree5(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success) //Y	
		     return RT_Fail;	

		   //compare and find min of 3 points. determine I/Q direction
		   if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		   	 return RT_Fail;

		   //increase step to find min value of this direction
		   if(R898_CompreStep(R898_Tuner_Num, &Compare_IQ[0], 29) != RT_Success)  //Y
			 return RT_Fail;	
		}
		else
		{
		   //if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Phase_Y, Compare_IQ[0].Gain_X, 0x11, &Compare_IQ[0]) != RT_Success) //X
		   if(R898_IQ_Tree5(R898_Tuner_Num, Compare_IQ[0].Phase_Y, Compare_IQ[0].Gain_X, 29, &Compare_IQ[0]) != RT_Success) //X
		     return RT_Fail;	
        
		   //compare and find min of 3 points. determine I/Q direction
		   if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		     return RT_Fail;

	       //increase step to find min value of this direction
		   if(R898_CompreStep(R898_Tuner_Num, &Compare_IQ[0], 28) != RT_Success) //X
		     return RT_Fail;	
		}
		

		//--- Check 3 points again---//
		if(X_Direction==1)
		{
		    if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Phase_Y, Compare_IQ[0].Gain_X, 29, &Compare_IQ[0]) != RT_Success) //X
			  return RT_Fail;	
		}
		else
		{
		   if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success) //Y
			return RT_Fail;		
		}

		if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
			return RT_Fail;

    //Section-9 check
    //if(R898_F_IMR(&Compare_IQ[0]) != RT_Success)
	if(R898_Section(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
			return RT_Fail;

	//clear IQ_Cap = 0   //  R27[1:0]  
	Compare_IQ[0].Iqcap = R898_T_Array[27] & 0xFC;

	if(R898_IMR_Iqcap(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
			return RT_Fail;

	*IQ_Pont = Compare_IQ[0];

	//reset gain/phase/iqcap control setting
	R898_I2C.RegAddr = 28; // R28[5:0]            
	R898_T_Array[28] = (R898_T_Array[28] & 0xC0);
	R898_I2C.Data = R898_T_Array[28];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 29; // R29[5:0]  
	R898_T_Array[29] = (R898_T_Array[29] & 0xC0) ;
	R898_I2C.Data =R898_T_Array[29]  ;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 27; // R27[1:0]  
	R898_T_Array[27] = (R898_T_Array[27] & 0xFC) ;
	R898_I2C.Data =R898_T_Array[27]  ;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	return RT_Success;
}



//--------------------------------------------------------------------------------------------
// Purpose: record IMC results by input gain/phase location
//          then adjust gain or phase positive 1 step and negtive 1 step, both record results
// input: FixPot: phase or gain
//        FlucPot phase or gain
//        PotReg: 0x10 or 0x11 for R898
//        CompareTree: 3 IMR trace and results
// output: TREU or FALSE
//--------------------------------------------------------------------------------------------

//20131217-Ryan


R898_ErrCode R898_IQ_Tree(R898_TUNER_NUM R898_Tuner_Num, UINT8 FixPot, UINT8 FlucPot, UINT8 PotReg, R898_SectType* CompareTree)
{
	UINT8 TreeCunt  = 0;
	UINT8 TreeTimes = 3;
	UINT8 PntReg    = 0;

	if(PotReg == 28)
		PntReg = 29; //phase control
	else
		PntReg = 28; //gain control

	for(TreeCunt = 0;TreeCunt < TreeTimes;TreeCunt ++)
	{
		R898_I2C.RegAddr = PotReg;
		R898_I2C.Data    = FixPot;
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		R898_I2C.RegAddr = PntReg;
		R898_I2C.Data    = FlucPot;
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		if(R898_Muti_Read(R898_Tuner_Num, &CompareTree[TreeCunt].Value) != RT_Success)
			return RT_Fail;
	

		if(PotReg == 28)
		{
			CompareTree[TreeCunt].Gain_X  = FixPot;
			CompareTree[TreeCunt].Phase_Y = FlucPot;
		}
		else
		{
			CompareTree[TreeCunt].Phase_Y  = FixPot;
			CompareTree[TreeCunt].Gain_X = FlucPot;
		}
		
		if(TreeCunt == 0)   //try right-side point
			//FlucPot ++; 
			FlucPot = FlucPot + (1<<1);
		else if(TreeCunt == 1) //try left-side point
		{
			//if((FlucPot & 0x1F) == 1) //if absolute location is 1, change I/Q direction
			if((FlucPot & 0x3E) == 3) //if absolute location is 1, change I/Q direction
			{
				//if(FlucPot & 0x20) //b[5]:I/Q selection. 0:Q-path, 1:I-path
				if(FlucPot & 0x01) //b[5]:I/Q selection. 0:Q-path, 1:I-path
				{
					//FlucPot = (FlucPot & 0xC0) | 0x01;	
					FlucPot = (FlucPot & 0xC0) | 0x02;	
				}
				else
				{
					//FlucPot = (FlucPot & 0xC0) | 0x21;
					FlucPot = (FlucPot & 0xC0) | 0x03;
				}
			}
			else
				FlucPot = FlucPot - 2;  
				
		}
	}

	return RT_Success;
}



R898_ErrCode R898_IQ_Tree5(R898_TUNER_NUM R898_Tuner_Num, UINT8 FixPot, UINT8 FlucPot, UINT8 PotReg, R898_SectType* CompareTree)
{
	UINT8 TreeCunt  = 0;
	UINT8 TreeTimes = 5;
	UINT8 TempPot   = 0;
	UINT8 PntReg    = 0;
	UINT8 CompCunt = 0;
	R898_SectType CorTemp[5];
    R898_SectType Compare_Temp;
	UINT8 CuntTemp = 0;

	memset(&Compare_Temp,0, sizeof(R898_SectType));
	Compare_Temp.Value = 255;

	for(CompCunt=0; CompCunt<3; CompCunt++)
	{
		CorTemp[CompCunt].Gain_X = CompareTree[CompCunt].Gain_X;
		CorTemp[CompCunt].Phase_Y = CompareTree[CompCunt].Phase_Y;
		CorTemp[CompCunt].Value = CompareTree[CompCunt].Value;
	}

	if(PotReg == 28)
		PntReg = 29; //phase control
	else
		PntReg = 28; //gain control

	for(TreeCunt = 0;TreeCunt < TreeTimes;TreeCunt ++)
	{
		R898_I2C.RegAddr = PotReg;
		R898_I2C.Data    = FixPot;
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		R898_I2C.RegAddr = PntReg;
		R898_I2C.Data    = FlucPot;
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		if(R898_Muti_Read(R898_Tuner_Num, &CorTemp[TreeCunt].Value) != RT_Success)
			return RT_Fail;
	
		if(PotReg == 28)
		{
			CorTemp[TreeCunt].Gain_X  = FixPot;
			CorTemp[TreeCunt].Phase_Y = FlucPot;
		}
		else
		{
			CorTemp[TreeCunt].Phase_Y  = FixPot;
			CorTemp[TreeCunt].Gain_X = FlucPot;
		}
		
		if(TreeCunt == 0)   //next try right-side 1 point
		{
			//FlucPot ++;     //+1
			FlucPot = FlucPot + (1<<1);     //+1
		}
		else if(TreeCunt == 1)   //next try right-side 2 point
		{
			//FlucPot ++;     //1+1=2
			FlucPot = FlucPot + (1<<1);     //1+1=2
		}
		else if(TreeCunt == 2)   //next try left-side 1 point
		{
			//if((FlucPot & 0x1F) == 0x02) //if absolute location is 2, change I/Q direction and set to 1
			if((FlucPot & 0x3E) == 0x04) //if absolute location is 2, change I/Q direction and set to 1
			{
				TempPot = 1;
				//if((FlucPot & 0x20)==0x20) //b[5]:I/Q selection. 0:Q-path, 1:I-path
				if((FlucPot & 0x01)==0x01) //b[5]:I/Q selection. 0:Q-path, 1:I-path
				{
					//FlucPot = (FlucPot & 0xC0) | 0x01;  //Q1
					FlucPot = (FlucPot & 0xC0) | 0x02;  //Q1
				}
				else
				{
					//FlucPot = (FlucPot & 0xC0) | 0x21;  //I1
					FlucPot = (FlucPot & 0xC0) | 0x03;  //I1
				}
			}
			else
				//FlucPot -= 3;  //+2-3=-1
				FlucPot = FlucPot - (3<<1) ;  //+2-3=-1
		}
		else if(TreeCunt == 3) //next try left-side 2 point
		{
			if(TempPot==1)  //been chnaged I/Q
			{
				//FlucPot += 1;
				FlucPot = FlucPot + (1<<1) ;
			}
			//else if((FlucPot & 0x1F) == 0x00) //if absolute location is 0, change I/Q direction
			else if((FlucPot & 0x3E) == 0x00) //if absolute location is 0, change I/Q direction
			{
				TempPot = 1;
				//if((FlucPot & 0x20)==0x20) //b[5]:I/Q selection. 0:Q-path, 1:I-path
				if((FlucPot & 0x01)==0x01) //b[5]:I/Q selection. 0:Q-path, 1:I-path
				{
					//FlucPot = (FlucPot & 0xC0) | 0x01;  //Q1
					FlucPot = (FlucPot & 0xC0) | 0x02;  //Q1
				}
				else
				{
					//FlucPot = (FlucPot & 0xC0) | 0x21;  //I1
					FlucPot = (FlucPot & 0xC0) | 0x03;  //I1
				}
			}
			else
				//FlucPot -= 1;  //-1-1=-2
			    FlucPot = FlucPot - (1<<1);  //-1-1=-2
		}

		if(CorTemp[TreeCunt].Value < Compare_Temp.Value)
		{
		  Compare_Temp.Value = CorTemp[TreeCunt].Value;
		  Compare_Temp.Gain_X = CorTemp[TreeCunt].Gain_X;
		  Compare_Temp.Phase_Y = CorTemp[TreeCunt].Phase_Y;		
		  CuntTemp = TreeCunt; 
		}
	}


	//CompareTree[0].Gain_X = CorTemp[CuntTemp].Gain_X;
	//CompareTree[0].Phase_Y = CorTemp[CuntTemp].Phase_Y;
	//CompareTree[0].Value = CorTemp[CuntTemp].Value;

	for(CompCunt=0; CompCunt<3; CompCunt++)
	{
		if(CuntTemp==3 || CuntTemp==4)
		{
			CompareTree[CompCunt].Gain_X = CorTemp[2+CompCunt].Gain_X;  //2,3,4
			CompareTree[CompCunt].Phase_Y = CorTemp[2+CompCunt].Phase_Y;
			CompareTree[CompCunt].Value = CorTemp[2+CompCunt].Value;
		}
		else
		{
			CompareTree[CompCunt].Gain_X = CorTemp[CompCunt].Gain_X;    //0,1,2
			CompareTree[CompCunt].Phase_Y = CorTemp[CompCunt].Phase_Y;
			CompareTree[CompCunt].Value = CorTemp[CompCunt].Value;
		}
		
	}

	return RT_Success;
}
//-----------------------------------------------------------------------------------/ 
// Purpose: compare IMC result aray [0][1][2], find min value and store to CorArry[0]
// input: CorArry: three IMR data array
// output: TRUE or FALSE
//-----------------------------------------------------------------------------------/
R898_ErrCode R898_CompreCor(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* CorArry)
{
	UINT8 CompCunt = 0;
	R898_SectType CorTemp;

	for(CompCunt = 3;CompCunt > 0;CompCunt --)
	{
		if(CorArry[0].Value > CorArry[CompCunt - 1].Value) //compare IMC result [0][1][2], find min value
		{
			CorTemp = CorArry[0];
			CorArry[0] = CorArry[CompCunt - 1];
			CorArry[CompCunt - 1] = CorTemp;
		}
	}

	return RT_Success;
}


//-------------------------------------------------------------------------------------//
// Purpose: if (Gain<9 or Phase<9), Gain+1 or Phase+1 and compare with min value
//          new < min => update to min and continue
//          new > min => Exit
// input: StepArry: three IMR data array
//        Pace: gain or phase register
// output: TRUE or FALSE 
//-------------------------------------------------------------------------------------//
R898_ErrCode R898_CompreStep(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* StepArry, UINT8 Pace)
{
	R898_SectType StepTemp;
	
	//min value already saved in StepArry[0]
	StepTemp.Phase_Y = StepArry[0].Phase_Y;
	StepTemp.Gain_X  = StepArry[0].Gain_X;
	//StepTemp.Iqcap  = StepArry[0].Iqcap;

	//while(((StepTemp.Gain_X & 0x1F) < R898_IMR_TRIAL) && ((StepTemp.Phase_Y & 0x1F) < R898_IMR_TRIAL))  
    while(((StepTemp.Gain_X & 0x3E) < R898_IMR_TRIAL) && ((StepTemp.Phase_Y & 0x3E) < R898_IMR_TRIAL))  
	{
		if(Pace == 28)	
			//StepTemp.Gain_X ++;
			StepTemp.Gain_X = StepTemp.Gain_X + (1<<1);
		else
			//StepTemp.Phase_Y ++;
			StepTemp.Phase_Y = StepTemp.Phase_Y + (1<<1);
	
		R898_I2C.RegAddr = 28;	
		R898_I2C.Data    = StepTemp.Gain_X ;
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		R898_I2C.RegAddr = 29;	
		R898_I2C.Data    = StepTemp.Phase_Y;
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		if(R898_Muti_Read(R898_Tuner_Num, &StepTemp.Value) != RT_Success)
			return RT_Fail;

		if(StepTemp.Value <= StepArry[0].Value)
		{
			StepArry[0].Gain_X  = StepTemp.Gain_X;
			StepArry[0].Phase_Y = StepTemp.Phase_Y;
			//StepArry[0].Iqcap = StepTemp.Iqcap;
			StepArry[0].Value   = StepTemp.Value;
		}
		else if((StepTemp.Value - 2*ADC_READ_COUNT) > StepArry[0].Value)
		{
			break;		
		}
		
	} //end of while()
	
	return RT_Success;
}


//-----------------------------------------------------------------------------------/ 
// Purpose: read multiple IMC results for stability
// input: IMR_Reg: IMC result address
//        IMR_Result_Data: result 
// output: TRUE or FALSE
//-----------------------------------------------------------------------------------/
R898_ErrCode R898_Muti_Read(R898_TUNER_NUM R898_Tuner_Num, UINT8* IMR_Result_Data)
{
#if (ADC_MULTI_READ==3)
	UINT8 ReadCunt     = 0;
	UINT16 ReadAmount  = 0;
	UINT8 ReadMax = 0;
	UINT8 ReadMin = 255;
	UINT8 ReadData = 0;
	
    R898_Delay_MS(R898_Tuner_Num, ADC_READ_DELAY);//2

	for(ReadCunt = 0; ReadCunt < (ADC_READ_COUNT+2); ReadCunt ++)
	{
		R898_I2C_Len.RegAddr = 0x00;
		R898_I2C_Len.Len = 7;              // read 0x06
		if(I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		{
			I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len);
		}

		ReadData = (R898_I2C_Len.Data[6] & 0x7F);
		
		ReadAmount = ReadAmount + (UINT16)ReadData;
		
		if(ReadData < ReadMin)
			ReadMin = ReadData;
		
        if(ReadData > ReadMax)
			ReadMax = ReadData;
	}
	*IMR_Result_Data = (UINT8) (ReadAmount - (UINT16)ReadMax - (UINT16)ReadMin);
#else
	

	R898_I2C_Len.RegAddr = 0x00;
	R898_I2C_Len.Len = 8;              // read 0x06
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];

	R898_Delay_MS(R898_Tuner_Num, ADC_READ_DELAY);//2
	if(I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
	{
		I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len);
	}
	*IMR_Result_Data = (R898_I2C_Len.Data[6] & 0x7F);
#endif

	return RT_Success;
}


R898_ErrCode R898_Section(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Pont)
{
	R898_SectType Compare_IQ[3];
	R898_SectType Compare_Bet[3];

	//Try X-1 column and save min result to Compare_Bet[0]
	//if((IQ_Pont->Gain_X & 0x1F) == 0x00)
	if((IQ_Pont->Gain_X & 0x3E) == 0x00)
	{
		//Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) & 0xDF) + 1;  //Q-path, Gain=1
		Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) & 0xFE) + (1<<1);  //Q-path, Gain=1
	}
	else
	{
		//Compare_IQ[0].Gain_X  = IQ_Pont->Gain_X - 1;  //left point
		Compare_IQ[0].Gain_X  = IQ_Pont->Gain_X - (1<<1);  //left point
	}
	Compare_IQ[0].Phase_Y = IQ_Pont->Phase_Y;

	//if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 0x10, &Compare_IQ[0]) != RT_Success)  // y-direction
	if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success)  // y-direction
		return RT_Fail;		

	if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;

	Compare_Bet[0].Gain_X = Compare_IQ[0].Gain_X;
	Compare_Bet[0].Phase_Y = Compare_IQ[0].Phase_Y;
	Compare_Bet[0].Value = Compare_IQ[0].Value;

	//Try X column and save min result to Compare_Bet[1]
	Compare_IQ[0].Gain_X = IQ_Pont->Gain_X;
	Compare_IQ[0].Phase_Y = IQ_Pont->Phase_Y;

	//if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 0x10, &Compare_IQ[0]) != RT_Success)
	if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;	

	if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;

	Compare_Bet[1].Gain_X = Compare_IQ[0].Gain_X;
	Compare_Bet[1].Phase_Y = Compare_IQ[0].Phase_Y;
	Compare_Bet[1].Value = Compare_IQ[0].Value;

	//Try X+1 column and save min result to Compare_Bet[2]
	//if((IQ_Pont->Gain_X & 0x1F) == 0x00)
	if((IQ_Pont->Gain_X & 0x3E) == 0x00)
		//Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) | 0x20) + 1;  //I-path, Gain=1
		Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) | 0x01) + (1<<1);  //I-path, Gain=1
	else
	    //Compare_IQ[0].Gain_X = IQ_Pont->Gain_X + 1;
		Compare_IQ[0].Gain_X = IQ_Pont->Gain_X + (1<<1);
	Compare_IQ[0].Phase_Y = IQ_Pont->Phase_Y;

	//if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 0x10, &Compare_IQ[0]) != RT_Success)
	if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;		

	if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;

	Compare_Bet[2].Gain_X = Compare_IQ[0].Gain_X;
	Compare_Bet[2].Phase_Y = Compare_IQ[0].Phase_Y;
	Compare_Bet[2].Value = Compare_IQ[0].Value;

	if(R898_CompreCor(R898_Tuner_Num, &Compare_Bet[0]) != RT_Success)
		return RT_Fail;

	*IQ_Pont = Compare_Bet[0];
	
	return RT_Success;
}


R898_ErrCode R898_IMR_Cross(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Pont, UINT8* X_Direct)
{

	R898_SectType Compare_Cross[9]; //(0,0)(0,Q-1)(0,I-1)(Q-1,0)(I-1,0)+(0,Q-2)(0,I-2)(Q-2,0)(I-2,0)
	R898_SectType Compare_Temp;
	UINT8 CrossCount = 0;
	UINT8  ADC_TEMP1 = 0;
	UINT8  ADC_TEMP2 = 0;


	UINT8 Reg28 = R898_T_Array[28] & 0xC0;	
	UINT8 Reg29 = R898_T_Array[29] & 0xC0;	

	memset(&Compare_Temp,0, sizeof(R898_SectType));
	Compare_Temp.Value = 255;

	for(CrossCount=0; CrossCount<9; CrossCount++)
	{

		if(CrossCount==0)
		{
		  Compare_Cross[CrossCount].Gain_X = Reg28;
		  Compare_Cross[CrossCount].Phase_Y = Reg29;
		}
		else if(CrossCount==1)
		{
		  Compare_Cross[CrossCount].Gain_X = Reg28;       //0
		  //Compare_Cross[CrossCount].Phase_Y = Reg29 + 1;  //Q-1
		  Compare_Cross[CrossCount].Phase_Y = Reg29 + (1<<1);  //Q-1
		}
		else if(CrossCount==2)
		{
		  Compare_Cross[CrossCount].Gain_X = Reg28;               //0
		  //Compare_Cross[CrossCount].Phase_Y = (Reg29 | 0x20) + 1; //I-1
		  Compare_Cross[CrossCount].Phase_Y = (Reg29 | 0x01) + (1<<1); //I-1
		}
		else if(CrossCount==3)
		{
		  //Compare_Cross[CrossCount].Gain_X = Reg28 + 1; //Q-1
		  Compare_Cross[CrossCount].Gain_X = Reg28 + (1<<1); //Q-1
		  Compare_Cross[CrossCount].Phase_Y = Reg29;
		}
		else if(CrossCount==4)
		{
		  //Compare_Cross[CrossCount].Gain_X = (Reg28 | 0x20) + 1; //I-1
		  Compare_Cross[CrossCount].Gain_X = (Reg28 | 0x01) + (1<<1); //I-1
		  Compare_Cross[CrossCount].Phase_Y = Reg29;
		}
		else if(CrossCount==5)
		{
		  Compare_Cross[CrossCount].Gain_X = Reg28;       //0
		  //Compare_Cross[CrossCount].Phase_Y = Reg29 + 2;  //Q-2
		  Compare_Cross[CrossCount].Phase_Y = Reg29 + (2<<1);  //Q-2
		}
		else if(CrossCount==6)
		{
		  Compare_Cross[CrossCount].Gain_X = Reg28;               //0
		  //Compare_Cross[CrossCount].Phase_Y = (Reg29 | 0x20) + 2; //I-2
		  Compare_Cross[CrossCount].Phase_Y = (Reg29 | 0x01) + (2<<1); //I-2
		}
		else if(CrossCount==7)
		{
		 // Compare_Cross[CrossCount].Gain_X = Reg28 + 2; //Q-2
		  Compare_Cross[CrossCount].Gain_X = Reg28 + (2<<1); //Q-2
		  Compare_Cross[CrossCount].Phase_Y = Reg29;
		}
		else if(CrossCount==8)
		{
		  //Compare_Cross[CrossCount].Gain_X = (Reg28 | 0x20) + 2; //I-2
		  Compare_Cross[CrossCount].Gain_X = (Reg28 | 0x01) + (2<<1); //I-2
		  Compare_Cross[CrossCount].Phase_Y = Reg29;
		}

    	R898_I2C.RegAddr = 28;	
	    R898_I2C.Data    = Compare_Cross[CrossCount].Gain_X;
	    if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		   return RT_Fail;

	    R898_I2C.RegAddr = 29;	
	    R898_I2C.Data    = Compare_Cross[CrossCount].Phase_Y;
	    if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		  return RT_Fail;

		if(CrossCount ==0)
			R898_Delay_MS(R898_Tuner_Num, 30);//2
	
        if(R898_Muti_Read(R898_Tuner_Num, &Compare_Cross[CrossCount].Value) != RT_Success)
		  return RT_Fail;

		ADC_TEMP1 = Compare_Cross[CrossCount].Value;

		if(R898_Muti_Read(R898_Tuner_Num, &Compare_Cross[CrossCount].Value) != RT_Success)
		  return RT_Fail;

		ADC_TEMP2 = Compare_Cross[CrossCount].Value;

		
		/*if(R898_Muti_Read(R898_Tuner_Num, &Compare_Cross[CrossCount].Value) != RT_Success)
		  return RT_Fail;

		ADC_TEMP3 = Compare_Cross[CrossCount].Value;*/

		//Compare_Cross[CrossCount].Value = (ADC_TEMP1+ADC_TEMP2+ADC_TEMP3)/3;
		Compare_Cross[CrossCount].Value = (ADC_TEMP1+ADC_TEMP2)/2;

		if( Compare_Cross[CrossCount].Value < Compare_Temp.Value)
		{
		  Compare_Temp.Value = Compare_Cross[CrossCount].Value;
		  Compare_Temp.Gain_X = Compare_Cross[CrossCount].Gain_X;
		  Compare_Temp.Phase_Y = Compare_Cross[CrossCount].Phase_Y;		
		}
	} //end for loop


    //if(((Compare_Temp.Phase_Y & 0x3F)==0x01) || (Compare_Temp.Phase_Y & 0x3F)==0x02)  //phase Q1 or Q2
	if(((Compare_Temp.Phase_Y & 0x3F)==0x02) || (Compare_Temp.Phase_Y & 0x3F)==0x04)  //phase Q1 or Q2
	{
      *X_Direct = (UINT8) 0;
	  IQ_Pont[0].Gain_X = Compare_Cross[0].Gain_X;    //0
	  IQ_Pont[0].Phase_Y = Compare_Cross[0].Phase_Y; //0
	  IQ_Pont[0].Value = Compare_Cross[0].Value;

	  IQ_Pont[1].Gain_X = Compare_Cross[1].Gain_X;    //0
	  IQ_Pont[1].Phase_Y = Compare_Cross[1].Phase_Y; //Q1
	  IQ_Pont[1].Value = Compare_Cross[1].Value;

	  IQ_Pont[2].Gain_X = Compare_Cross[5].Gain_X;   //0
	  IQ_Pont[2].Phase_Y = Compare_Cross[5].Phase_Y;//Q2
	  IQ_Pont[2].Value = Compare_Cross[5].Value;
	}
	//else if(((Compare_Temp.Phase_Y & 0x3F)==0x21) || (Compare_Temp.Phase_Y & 0x3F)==0x22)  //phase I1 or I2
	else if(((Compare_Temp.Phase_Y & 0x3F)==0x03) || (Compare_Temp.Phase_Y & 0x3F)==0x05)  //phase I1 or I2
	{
      *X_Direct = (UINT8) 0;
	  IQ_Pont[0].Gain_X = Compare_Cross[0].Gain_X;    //0
	  IQ_Pont[0].Phase_Y = Compare_Cross[0].Phase_Y; //0
	  IQ_Pont[0].Value = Compare_Cross[0].Value;

	  IQ_Pont[1].Gain_X = Compare_Cross[2].Gain_X;    //0
	  IQ_Pont[1].Phase_Y = Compare_Cross[2].Phase_Y; //Q1
	  IQ_Pont[1].Value = Compare_Cross[2].Value;

	  IQ_Pont[2].Gain_X = Compare_Cross[6].Gain_X;   //0
	  IQ_Pont[2].Phase_Y = Compare_Cross[6].Phase_Y;//Q2
	  IQ_Pont[2].Value = Compare_Cross[6].Value;
	}
	//else if(((Compare_Temp.Gain_X & 0x3F)==0x01) || (Compare_Temp.Gain_X & 0x3F)==0x02)  //gain Q1 or Q2
	else if(((Compare_Temp.Gain_X & 0x3F)==0x02) || (Compare_Temp.Gain_X & 0x3F)==0x04)  //gain Q1 or Q2
	{
      *X_Direct = (UINT8) 1;
	  IQ_Pont[0].Gain_X = Compare_Cross[0].Gain_X;    //0
	  IQ_Pont[0].Phase_Y = Compare_Cross[0].Phase_Y; //0
	  IQ_Pont[0].Value = Compare_Cross[0].Value;

	  IQ_Pont[1].Gain_X = Compare_Cross[3].Gain_X;    //Q1
	  IQ_Pont[1].Phase_Y = Compare_Cross[3].Phase_Y; //0
	  IQ_Pont[1].Value = Compare_Cross[3].Value;

	  IQ_Pont[2].Gain_X = Compare_Cross[7].Gain_X;   //Q2
	  IQ_Pont[2].Phase_Y = Compare_Cross[7].Phase_Y;//0
	  IQ_Pont[2].Value = Compare_Cross[7].Value;
	}
	//else if(((Compare_Temp.Gain_X & 0x3F)==0x21) || (Compare_Temp.Gain_X & 0x3F)==0x22)  //gain I1 or I2
	else if(((Compare_Temp.Gain_X & 0x3F)==0x03) || (Compare_Temp.Gain_X & 0x3F)==0x05)  //gain I1 or I2
	{
      *X_Direct = (UINT8) 1;
	  IQ_Pont[0].Gain_X = Compare_Cross[0].Gain_X;    //0
	  IQ_Pont[0].Phase_Y = Compare_Cross[0].Phase_Y; //0
	  IQ_Pont[0].Value = Compare_Cross[0].Value;

	  IQ_Pont[1].Gain_X = Compare_Cross[4].Gain_X;    //I1
	  IQ_Pont[1].Phase_Y = Compare_Cross[4].Phase_Y; //0
	  IQ_Pont[1].Value = Compare_Cross[4].Value;

	  IQ_Pont[2].Gain_X = Compare_Cross[8].Gain_X;   //I2
	  IQ_Pont[2].Phase_Y = Compare_Cross[8].Phase_Y;//0
	  IQ_Pont[2].Value = Compare_Cross[8].Value;
	}
	else //(0,0) 
	{	
	  *X_Direct = (UINT8) 1;
	  IQ_Pont[0].Gain_X = Compare_Cross[0].Gain_X;
	  IQ_Pont[0].Phase_Y = Compare_Cross[0].Phase_Y;
	  IQ_Pont[0].Value = Compare_Cross[0].Value;

	  IQ_Pont[1].Gain_X = Compare_Cross[3].Gain_X;    //Q1
	  IQ_Pont[1].Phase_Y = Compare_Cross[3].Phase_Y; //0
	  IQ_Pont[1].Value = Compare_Cross[3].Value;

	  IQ_Pont[2].Gain_X = Compare_Cross[4].Gain_X;   //I1
	  IQ_Pont[2].Phase_Y = Compare_Cross[4].Phase_Y; //0
	  IQ_Pont[2].Value = Compare_Cross[4].Value;
	}
	return RT_Success;
}


//----------------------------------------------------------------------------------------//
// purpose: search surrounding points from previous point 
//          try (x-1), (x), (x+1) columns, and find min IMR result point
// input: IQ_Pont: previous point data(IMR Gain, Phase, ADC Result, RefRreq)
//                 will be updated to final best point                 
// output: TRUE or FALSE
//----------------------------------------------------------------------------------------//
R898_ErrCode R898_F_IMR(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Pont)
{
	R898_SectType Compare_IQ[3];
	R898_SectType Compare_Bet[3];


	//VGA
	/*for(VGA_Count=11; VGA_Count < 16; VGA_Count ++)
	{
		R898_I2C.RegAddr = 36;	//  R36[7:4]  
		R898_I2C.Data    = ((R898_T_Array[36] & 0x0F) | VGA_Count);
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;

		R898_Delay_MS(R898_Tuner_Num, 10);
		
		if(R898_Muti_Read(R898_Tuner_Num, &VGA_Read) != RT_Success)
			return RT_Fail;

		if(VGA_Read > 80*ADC_READ_COUNT)
		break;
	}*/

	//Try X-1 column and save min result to Compare_Bet[0]
	//if((IQ_Pont->Gain_X & 0x1F) == 0x00)
	if((IQ_Pont->Gain_X & 0x3E) == 0x00)
	{
		//Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) & 0xDF) + 1;  //Q-path, Gain=1
		Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) & 0xFE) + (1<<1);  //Q-path, Gain=1
	}
	else
	{
		//Compare_IQ[0].Gain_X  = IQ_Pont->Gain_X - 1;  //left point
		Compare_IQ[0].Gain_X  = IQ_Pont->Gain_X - (1<<1);  //left point
	}
	Compare_IQ[0].Phase_Y = IQ_Pont->Phase_Y;

	//if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 0x10, &Compare_IQ[0]) != RT_Success)  // y-direction
	if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success)  // y-direction
		return RT_Fail;	

	if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;

	Compare_Bet[0].Gain_X = Compare_IQ[0].Gain_X;
	Compare_Bet[0].Phase_Y = Compare_IQ[0].Phase_Y;
	Compare_Bet[0].Value = Compare_IQ[0].Value;

	//Try X column and save min result to Compare_Bet[1]
	Compare_IQ[0].Gain_X = IQ_Pont->Gain_X;
	Compare_IQ[0].Phase_Y = IQ_Pont->Phase_Y;

	//if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 0x10, &Compare_IQ[0]) != RT_Success)
	if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;	

	if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;

	Compare_Bet[1].Gain_X = Compare_IQ[0].Gain_X;
	Compare_Bet[1].Phase_Y = Compare_IQ[0].Phase_Y;
	Compare_Bet[1].Value = Compare_IQ[0].Value;

	//Try X+1 column and save min result to Compare_Bet[2]
	if((IQ_Pont->Gain_X & 0x1F) == 0x00)		
		//Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) | 0x20) + 1;  //I-path, Gain=1
		Compare_IQ[0].Gain_X = ((IQ_Pont->Gain_X) | 0x01) + (1<<1);  //I-path, Gain=1
	else
	    //Compare_IQ[0].Gain_X = IQ_Pont->Gain_X + 1;
		Compare_IQ[0].Gain_X = IQ_Pont->Gain_X + (1<<1);
	Compare_IQ[0].Phase_Y = IQ_Pont->Phase_Y;

	//if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 0x10, &Compare_IQ[0]) != RT_Success)
	if(R898_IQ_Tree(R898_Tuner_Num, Compare_IQ[0].Gain_X, Compare_IQ[0].Phase_Y, 28, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;		

	if(R898_CompreCor(R898_Tuner_Num, &Compare_IQ[0]) != RT_Success)
		return RT_Fail;

	Compare_Bet[2].Gain_X = Compare_IQ[0].Gain_X;
	Compare_Bet[2].Phase_Y = Compare_IQ[0].Phase_Y;
	Compare_Bet[2].Value = Compare_IQ[0].Value;

	if(R898_CompreCor(R898_Tuner_Num, &Compare_Bet[0]) != RT_Success)
		return RT_Fail;

	//clear IQ_Cap = 0
	Compare_Bet[0].Iqcap = R898_T_Array[27] & 0xFC;	//  R27[1:0] 

	if(R898_IMR_Iqcap(R898_Tuner_Num, &Compare_Bet[0]) != RT_Success)
		return RT_Fail;

	*IQ_Pont = Compare_Bet[0];
	
	return RT_Success;
}


R898_ErrCode R898_IMR_Iqcap(R898_TUNER_NUM R898_Tuner_Num, R898_SectType* IQ_Point)   
{
    R898_SectType Compare_Temp;
	int i = 0;

	/*//Set Gain/Phase to right setting
	R898_I2C.RegAddr = 0x10;	// R16[5:0]  
	R898_I2C.Data = IQ_Point->Gain_X; 
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 0x11;	// R17[5:0]  
	R898_I2C.Data = IQ_Point->Phase_Y;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;*/

	//Set Gain/Phase to right setting
	R898_I2C.RegAddr = 28; // R28[5:0]            
	R898_I2C.Data = IQ_Point->Gain_X;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 29; // R29[5:0]  
	R898_I2C.Data = IQ_Point->Phase_Y  ;
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	

	//try iqcap
	for(i=0; i<3; i++)	
	{
		Compare_Temp.Iqcap = (UINT8) i;  
		R898_I2C.RegAddr = 27; // R27[1:0]  
		R898_T_Array[27] = (R898_T_Array[27] & 0xFC) | Compare_Temp.Iqcap;  
		R898_I2C.Data =R898_T_Array[27]  ;
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			   return RT_Fail;

		if(R898_Muti_Read(R898_Tuner_Num, &(Compare_Temp.Value)) != RT_Success)
			   return RT_Fail;

		if(Compare_Temp.Value < IQ_Point->Value)
		{
			IQ_Point->Value = Compare_Temp.Value; 
			IQ_Point->Iqcap = Compare_Temp.Iqcap;
		}
	}

    return RT_Success;
}


R898_ErrCode R898_SetStandard(R898_TUNER_NUM R898_Tuner_Num, R898_Standard_Type RT_Standard)
{	 
	
	UINT8 u1FilCalGap = 40;
	if(RT_Standard != R898_pre_standard[R898_Tuner_Num])
	{
		 if(R898_InitReg(R898_Tuner_Num, RT_Standard) != RT_Success)      
		     return RT_Fail;
	}
    R898_pre_standard[R898_Tuner_Num] = RT_Standard;

	Sys_Info1 = R898_Sys_Sel(RT_Standard);

	// Filter Calibration
	//UINT8 u1FilCalGap = 8;

	if(RT_Standard<R898_ATV_SIZE)    //ATV
	    u1FilCalGap = R898_Fil_Cal_Gap;
	else
	    u1FilCalGap = 8;



    if(R898_Fil_Cal_flag[R898_Tuner_Num][RT_Standard] == FALSE)
	{
		R898_Fil_Cal_code[R898_Tuner_Num][RT_Standard] = R898_Filt_Cal_ADC(R898_Tuner_Num, Sys_Info1.FILT_CAL_IF, Sys_Info1.BW, u1FilCalGap);
		R898_Fil_Cal_BW[R898_Tuner_Num][RT_Standard] = R898_Bandwidth[R898_Tuner_Num];
        R898_Fil_Cal_flag[R898_Tuner_Num][RT_Standard] = TRUE;
		if(R898_IMR_Cal_Result[R898_Tuner_Num]==1) //fail
		{
			if((R898_Fil_Cal_BW[R898_Tuner_Num][RT_Standard]==0x60) && (R898_Fil_Cal_code[R898_Tuner_Num][RT_Standard]==15))  //6M/15
			{
				R898_Fil_Cal_BW[R898_Tuner_Num][RT_Standard] = R898_Fil_Cal_BW_def[RT_Standard];
				R898_Fil_Cal_code[R898_Tuner_Num][RT_Standard] = R898_Fil_Cal_code_def[RT_Standard];
			}
		}
	    //Reset register and Array 
	    if(R898_InitReg(R898_Tuner_Num, RT_Standard) != RT_Success)
		   return RT_Fail;
	}

	R898_I2C.I2cAddr=R898_ADDRESS[R898_TUNER];
	
	// Set LPF fine code
	R898_I2C.RegAddr = 35;	//  R898[7:3]  
	R898_T_Array[35] = (R898_T_Array[35] & 0x07) | (R898_Fil_Cal_code[R898_Tuner_Num][RT_Standard])<<3;  
	R898_I2C.Data = R898_T_Array[35];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	// Set LPF coarse BW
	R898_I2C.RegAddr = 30;	// R30[2:1]  
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF9) | R898_Fil_Cal_BW[R898_Tuner_Num][RT_Standard];
	R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;


	// Set LPF fine code
	R898_I2C.RegAddr = 36;	
	R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF0) | Sys_Info1.HPF_COR_R36_0_4BT ;  
	R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
	if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		return RT_Fail;

	return RT_Success;
}


UINT8  R898_Filt_Cal_ADC(R898_TUNER_NUM R898_Tuner_Num, UINT32 IF_Freq, UINT8 R898_BW, UINT8 FilCal_Gap)
{
	 UINT8     u1FilterCodeResult = 0;
	 UINT8	   u1FilterCodeResult_flag = 0;
	 UINT8     u1FilterCode = 0;
	 UINT32    u4RingFreq = 172000;
	 UINT8     u1FilterCalValue = 0;
	 UINT8     u1FilterCalValuePre = 0;
	 UINT8     initial_cnt = 0;
	 UINT8     i = 0;

	 R898_Standard_Type	R898_Standard; 
	 UINT8   VGA_Count = 0; 
	 UINT8   VGA_Read = 0; 
	 UINT8 XtalCap, CapTot;
	 
	 UINT8 InitArrayCunt = 0;
	
	/*if(R898_Xtal==16000)  //16M
	{
         divnum_ring = 11;
	}
	else if (R898_Xtal==24000)  //24M
	{
		 divnum_ring = 2;
	}
	else
	{
		divnum_ring = 0;
	}
	 RingVCO = (16+divnum_ring)* 8 * RingRef;
	 u4RingFreq = RingVCO/48;*/



	 //R898_Standard_Type	R898_Standard;
	 R898_Standard=R898_ATSC; //no set R898_SATELLITE


	 
	if(R898_XTAL_CAP>30)
	{
		CapTot = R898_XTAL_CAP-10;
		XtalCap = 1;  //10
	}
	else
	{
		CapTot = R898_XTAL_CAP;
		XtalCap = 0;  //0
	}

	R898_T_LPF_CAL_172MHz[33]=(R898_T_LPF_CAL_172MHz[33] & 0xC0) |  (XtalCap<<5) | CapTot;

	
	
	R898_I2C_Len.RegAddr = 0x00;   //  R898_T:0x00
	R898_I2C_Len.Len = R898_T_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];

	for(InitArrayCunt = 0; InitArrayCunt<R898_T_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_T_LPF_CAL_172MHz[InitArrayCunt];
		R898_T_Array[InitArrayCunt] = R898_T_LPF_CAL_172MHz[InitArrayCunt];
	}

	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;



	R898_I2C_Len.RegAddr = 0x00;   //  R898_S:0x00
	R898_I2C_Len.Len = R898_S_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_SATELITE];

	for(InitArrayCunt = 0; InitArrayCunt<R898_S_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_S_LPF_CAL_172MHz[InitArrayCunt];
		R898_S_Array[InitArrayCunt] = R898_S_LPF_CAL_172MHz[InitArrayCunt];
	}

	
	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;



	//Must do before PLL() 
	if(R898_MUX(R898_Tuner_Num, u4RingFreq + IF_Freq, u4RingFreq, R898_STD_SIZE) != RT_Success)     //FilCal MUX (LO_Freq, RF_Freq)
		return RT_Fail;

	//Set PLL
	if(R898_PLL(R898_Tuner_Num, (u4RingFreq + IF_Freq), R898_STD_SIZE) != RT_Success)   //FilCal PLL
		  return RT_Fail;

	 for(VGA_Count=0; VGA_Count < 16; VGA_Count ++)
	 {
		R898_I2C.RegAddr = 36;	//  R36[7:4]  
		R898_I2C.Data    = (R898_T_Array[36] & 0x0F) + (VGA_Count<<4);  
		if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
			return RT_Fail;


		R898_Delay_MS(R898_Tuner_Num, VGA_DELAY); //

		
		if(R898_Muti_Read(R898_Tuner_Num, &VGA_Read) != RT_Success)
			return RT_Fail;

		if(VGA_Read > 80*ADC_READ_COUNT)
			break;
		if(VGA_Count==15 && VGA_Read < 40*ADC_READ_COUNT)
		{
			R898_I2C.RegAddr = 31;	//  R36[6]  
			R898_I2C.Data    = (R898_T_Array[31] | 0x04) ;  
			if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
				return RT_Fail;
			for(VGA_Count=12; VGA_Count < 16; VGA_Count ++)
			{
				R898_I2C.RegAddr = 36;	//  R36[7:4]  
				R898_I2C.Data    = (R898_T_Array[36] & 0x0F) + (VGA_Count<<4);  
				if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
					return RT_Fail;
				R898_Delay_MS(R898_Tuner_Num, VGA_DELAY); //
				if(R898_Muti_Read(R898_Tuner_Num, &VGA_Read) != RT_Success)
					return RT_Fail;
				if(VGA_Read > 80*ADC_READ_COUNT)
					break;
			 }
	 }
	 }

	 //------- Try suitable BW --------//

	 if(R898_BW==0x60) //6M
         initial_cnt = 1;  //try 7M first
	 else
		 initial_cnt = 0;  //try 8M first

	 for(i=initial_cnt; i<3; i++)
	 {
         
		 if(u1FilterCode==31)
			u1FilterCode = 0;
		 
		 if(i==0)
		 {
             R898_Bandwidth[R898_Tuner_Num] = 0x02; //8M   
			 R898_T_Array[30] = (R898_T_Array[30] & 0xF9) | 0x02;
		 }
		 else if(i==1)
		 {
			 R898_Bandwidth[R898_Tuner_Num] = 0x04; //7M   
			 R898_T_Array[30] = (R898_T_Array[30] & 0xF9) | 0x04;	  
		 }
		 else
		 {
			 R898_Bandwidth[R898_Tuner_Num] = 0x06; //6M    
			 R898_T_Array[30] = (R898_T_Array[30] & 0xF9) | 0x06;	
		 }

		 R898_I2C.RegAddr = 30; 
         R898_I2C.Data = R898_T_Array[30];		
         if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
		      return RT_Fail;	


		u1FilterCalValuePre = 0;
		for(u1FilterCode=0; u1FilterCode<31; u1FilterCode++)
		{
			 R898_I2C.RegAddr = 35;	//BW tune  R35[7:3]  
			 R898_T_Array[35] = (R898_T_Array[35] & 0x07) | (u1FilterCode<<3);
			 R898_I2C.Data = R898_T_Array[35];
			 if(I2C_Write(R898_Tuner_Num, &R898_I2C) != RT_Success)
				  return RT_Fail;

			 R898_Delay_MS(R898_Tuner_Num, FILTER_DELAY); //delay ms

			 if(R898_Muti_Read(R898_Tuner_Num, &u1FilterCalValue) != RT_Success)
				  return RT_Fail;

			 if(u1FilterCode==0)
				  u1FilterCalValuePre = u1FilterCalValue;

			 //*if((u1FilterCalValue+FilCal_Gap*ADC_READ_COUNT) < u1FilterCalValuePre)
			 //if((u1FilterCalValue+FilCal_Gap*ADC_READ_COUNT) < VGA_Read)
			 if((u1FilterCalValue+20) < VGA_Read)
			 {
				 u1FilterCodeResult = u1FilterCode;
				 u1FilterCodeResult_flag =1 ;
				  break;
			 }

		}

	

		if(u1FilterCodeResult_flag ==1)
			break;

	 }

     //-------- Try LPF filter code ---------//

	 if(u1FilterCode==31)
          u1FilterCodeResult = 30;

	  return u1FilterCodeResult;

}
R898_ErrCode R898_SetFrequency(R898_TUNER_NUM R898_Tuner_Num, R898_Set_Info R898_INFO)
{

	 UINT32	LO_KHz;
	 UINT8 InitArrayCunt = 0;
	 R898_I2C.I2cAddr=R898_ADDRESS[R898_SATELITE];
	 R898_I2C.RegAddr = 15;
	 R898_S_Array[15] = R898_S_Array[15] & 0xFE;
	 R898_I2C.Data = R898_S_Array[15];
	 if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
			 return RT_Fail;
	 R898_I2C.I2cAddr=R898_ADDRESS[R898_TUNER];

	



	


     SysFreq_Info1 = R898_SysFreq_Sel(R898_INFO.R898_Standard, R898_INFO.RF_KHz);

	 if(R898_INFO.R898_Standard!=R898_SATELLITE)
	 {
		 // Check Input Frequency Range
		 if((R898_INFO.RF_KHz<40000) || (R898_INFO.RF_KHz>1002000))
		 {
				  return RT_Fail;
		 }
	 }
	 else
	 {
		// Check Input Frequency Range
		 if((R898_INFO.RF_KHz<40000) || (R898_INFO.RF_KHz>2400000))
		 {
				  return RT_Fail;
		 }
	 }

	      LO_KHz = R898_INFO.RF_KHz + Sys_Info1.IF_KHz;

	 //Set MUX dependent var. Must do before PLL( ) 
     if(R898_MUX(R898_Tuner_Num, LO_KHz, R898_INFO.RF_KHz, R898_INFO.R898_Standard) != RT_Success)   //normal MUX
        return RT_Fail;


     //Set PLL
     if(R898_PLL(R898_Tuner_Num, LO_KHz, R898_INFO.R898_Standard) != RT_Success) //noraml PLL
        return RT_Fail;


	 //Set TF
	 if(R898_SetTF(R898_Tuner_Num, R898_INFO.RF_KHz, R898_SetTfType_UL_MID) != RT_Success)
		return RT_Fail;

	 R898_I2C.RegAddr = 21;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF7) | (SysFreq_Info1.QCTRL_29_R21_3_1BT<<3);	
	
	 R898_I2C.RegAddr = 21;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFB) | (SysFreq_Info1.LNA_TF_QCTRL_R21_2_1BT<<2);	
	 R898_I2C.RegAddr = 20;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFC) | (SysFreq_Info1.LNA_MAX_GAIN_R20_0_2BT);	
	 R898_I2C.RegAddr = 21;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFE) | (SysFreq_Info1.LNA_MIN_GAIN_R21_0_1BT);	
	 R898_I2C.RegAddr = 51;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF0) | (7-SysFreq_Info1.LNA_TOP_R51_0_3BT) | (SysFreq_Info1.EXTRA_LNA_TOP_R51_3_1BT<<3);	
	 R898_I2C.RegAddr = 19;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x87) | ((SysFreq_Info1.LNA_VTH_R19_3_4BT)<<3);	
	 R898_I2C.RegAddr = 48;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x0F) | ((SysFreq_Info1.LNA_VTL_R48_4_4BT)<<4);	
	 R898_I2C.RegAddr = 51;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x8F) | ((7-SysFreq_Info1.RF_TOP_R51_4_3BT)<<4);
	 R898_I2C.RegAddr = 24;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xE1) | (SysFreq_Info1.RF_VTH_R24_1_4BT<<1);	
	 R898_I2C.RegAddr = 49;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x0F) | ((SysFreq_Info1.RF_VTL_R49_4_4BT)<<4);	
	 R898_I2C.RegAddr = 52;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF0) | (SysFreq_Info1.NRB_TOP_R52_0_4BT);	
	 R898_I2C.RegAddr = 52;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xCF) | ((SysFreq_Info1.NRB_BW_LPF_R52_4_2BT)<<4);
	 R898_I2C.RegAddr = 38;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF3) | ((SysFreq_Info1.NRB_DET_HPF_R38_2_2BT)<<2);
	 R898_I2C.RegAddr = 54;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF1) | ((7-SysFreq_Info1.MIXER_TOP_R54_1_3BT)<<1);
	 R898_I2C.RegAddr = 25;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x0F) | ((SysFreq_Info1.MIXER_VTH_R25_4_4BT)<<4);	
	 R898_I2C.RegAddr = 50;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x0F) | ((SysFreq_Info1.MIXER_VTL_R50_4_4BT)<<4);
	 R898_I2C.RegAddr = 29;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x3F) | ((SysFreq_Info1.FILTER_TOP_R29_6_2BT)<<6);
	 R898_I2C.RegAddr = 36;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x0F) | ((SysFreq_Info1.FILTER_VTH_R36_4_4BT)<<4);	
	 R898_I2C.RegAddr = 48;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF8) | ((SysFreq_Info1.MIXG_LIMIT_R48_0_3BT));
	 R898_I2C.RegAddr = 27;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xCF) | ((SysFreq_Info1.LIMITG_IMG_ADD_R27_4_2BT)<<4);
	 R898_I2C.RegAddr = 26;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x1F) | ((SysFreq_Info1.MIXER_AMP_LPF_R26_5_3BT)<<5);	
	 R898_I2C.RegAddr = 44;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF3) | ((SysFreq_Info1.MIXER_LPF_ADD_R44_2_2BT)<<2);
	 R898_I2C.RegAddr = 49;	
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFC) | (SysFreq_Info1.RF_BUF_LPF_R49_0_2BT);
	 R898_I2C.RegAddr = 25;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFE) | (SysFreq_Info1.RBG_MIN_RFBUF_R25_0_1BT);
	 R898_I2C.RegAddr = 24;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x7F) | ((SysFreq_Info1.RBG_MAX_RFBUF_R24_7_1BT)<<7);
	 R898_I2C.RegAddr = 53;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xDF) | ((SysFreq_Info1.LNA_DIS_MODE_R53_5_1BT)<<5);
	 R898_I2C.RegAddr = 53;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xBF) | ((SysFreq_Info1.RF_DIS_MODE_R53_6_1BT)<<6);
	 R898_I2C.RegAddr = 54;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFE) | (SysFreq_Info1.LNA_DIS_CURR_R54_0_1BT);
	 R898_I2C.RegAddr = 53;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFE) | (SysFreq_Info1.RF_DIS_CURR_R53_0_1BT);
	 R898_I2C.RegAddr = 55;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFC) | (SysFreq_Info1.LNA_RF_DIS_SLOW_R55_0_2BT);
	 R898_I2C.RegAddr = 55;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF3) | ((SysFreq_Info1.LNA_RF_DIS_FAST_R55_2_2BT)<<2);
	 R898_I2C.RegAddr = 56;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFD) | ((SysFreq_Info1.SLOW_DIS_ENABLE_R56_1_1BT)<<1);
	 R898_I2C.RegAddr = 55;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xBF) | ((SysFreq_Info1.BB_DIS_CURR_R55_6_1BT)<<6);
	 R898_I2C.RegAddr = 55;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xCF) | ((SysFreq_Info1.MIXER_FILTER_DIS_R55_4_2BT)<<4);
	 R898_I2C.RegAddr = 56;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xEF) | ((SysFreq_Info1.FAGC_2_SAGC_R56_4_1BT)<<4);
	 R898_I2C.RegAddr = 58;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x3F) | ((SysFreq_Info1.CLK_FAST_R58_6_2BT)<<6);
	 R898_I2C.RegAddr = 58;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xCF) | ((SysFreq_Info1.CLK_SLOW_R58_4_2BT)<<4);
	 R898_I2C.RegAddr = 57;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xEF) | ((SysFreq_Info1.LEVEL_SW_R57_4_1BT)<<4);
	 R898_I2C.RegAddr = 57;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x7F) | ((SysFreq_Info1.ATV_MODE_SEL_R57_7_1BT)<<7);
	 R898_I2C.RegAddr = 57;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xDF) | ((SysFreq_Info1.LEVEL_SW_VTHH_R57_5_1BT)<<5);
	 R898_I2C.RegAddr = 57;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xBF) | ((SysFreq_Info1.LEVEL_SW_VTLL_R57_6_1BT)<<6);
	 R898_I2C.RegAddr = 47;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFD) | ((SysFreq_Info1.FILTER_G_CONTROL_R47_1_1BT)<<1);
	 R898_I2C.RegAddr = 31;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x3F) | (SysFreq_Info1.RF_WIDEN_VTH_VTL_R31_6_2BT<<6);
	 R898_I2C.RegAddr = 47;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xCF) | (SysFreq_Info1.BB_WIDEN_VTH_VTL_R47_4_2BT<<4);
	 R898_I2C.RegAddr = 17;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF3) | (SysFreq_Info1.IMG_NRB_ADDER_R17_2_2BT<<2);
	 R898_I2C.RegAddr = 28;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x7F) | (SysFreq_Info1.LPF_3TH_HGAIN_R28_7_1BT<<7);
	 R898_I2C.RegAddr = 37;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x3F) | (SysFreq_Info1.LPF_3TH_GAIN_R37_6_2BT<<6);
	 R898_I2C.RegAddr = 26;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xEF) | (SysFreq_Info1.VGA_INR_R26_4_1BT<<4);
	 R898_I2C.RegAddr = 47;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0x9F) | (SysFreq_Info1.VGA_INC_R47_5_2BT<<5);
	 R898_I2C.RegAddr = 31;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xF7) | (SysFreq_Info1.ATTUN_VGA_R31_3_1BT<<3);
	 R898_I2C.RegAddr = 31;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xFB) | (SysFreq_Info1.VGA_SHIFTGAIN_R31_2_1BT<<2);
	 R898_I2C.RegAddr = 31;	  
	 R898_T_Array[R898_I2C.RegAddr] = (R898_T_Array[R898_I2C.RegAddr] & 0xEF) | (SysFreq_Info1.VGA_ATT5DB_R31_4_1BT<<4);



	
	 R898_I2C_Len.RegAddr = 0x00;   //  R898_T:0x10
	 R898_I2C_Len.Len = R898_T_REG_NUM;
	 R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];
	 for(InitArrayCunt = 0; InitArrayCunt<R898_T_REG_NUM; InitArrayCunt ++)
	 {
		 R898_I2C_Len.Data[InitArrayCunt] = R898_T_Array[InitArrayCunt];
	 }
	 if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		 return RT_Fail;	
	 R898_T_SBY_FLAG[R898_Tuner_Num] = FALSE;
	 return RT_Success;
}


R898_ErrCode R898_Satellite_Setting(R898_TUNER_NUM R898_Tuner_Num, R898_Set_Info R898_INFO)
{

		UINT8 fil_cnt=0;
		UINT8 icount=0;

		R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_SATELITE];
		R898_I2C.I2cAddr=R898_ADDRESS[R898_SATELITE];





		if(R898_S_Initial_done_flag[R898_Tuner_Num] == FALSE)
		{
			R898_pre_satellite_bw[R898_Tuner_Num] = 0; //Init BW Value
			R898_satellite_bw = 0;

			for(icount=0;icount<R898_S_REG_NUM;icount++)
			{

				R898_S_Array[icount] = R898_S_iniArray[icount];
			}	
		
	

			//Output Signal Mode   R14[0]
			if(R898_INFO.R898_SATELLITE_OutputSignal_Mode != DIFFERENTIALOUT)
			{
				R898_S_Array[14] |=0x01;
			}
			else
			{
				R898_S_Array[14] &=0xFE;
			}
		
			//AGC Type  //R13[6] Negative=0 ; Positive=1;
			if(R898_INFO.R898_SATELLITE_AGC_Mode != AGC_POSITIVE)
			{
				R898_S_Array[13] &=0xBF;
			}
			else
			{
				R898_S_Array[13] |=0x40;
			}

			//Atten 6dB Type   //R13[7] 0dB=0 ; 6dB=1;
			R898_S_Array[13] = (R898_S_Array[13] & 0x7F) |( R898_INFO.R898_SATELLITE_6dB <<7);

			//Atten 3.5dB Type  //R14[1] 0dB=0 ; 3.5dB=1;
			R898_S_Array[14] = (R898_S_Array[14] & 0xFD) |( R898_INFO.R898_SATELLITE_3_5dB <<1);


			
			R898_I2C_Len.RegAddr=0x00;
			R898_I2C_Len.Len=0x20;
			for(int i=0;i<R898_I2C_Len.Len;i++)
			{
				R898_I2C_Len.Data[i]=R898_S_Array[i];
			}
		

			//if(I2C_Write_Len(TUNER_NO,&R898_I2C_Len) != RT_Success)
			if(I2C_Write_Len(R898_Tuner_Num,&R898_I2C_Len) != RT_Success)
				return RT_Fail;

			R898_S_Initial_done_flag[R898_Tuner_Num] = TRUE;
		}
		R898_I2C.RegAddr = 15;
		R898_S_Array[15] = R898_S_Array[15] | 0x01;
		R898_I2C.Data = R898_S_Array[15];
		if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
				return RT_Fail;

	


		// Check Input Frequency Range
		if((R898_INFO.RF_KHz<200000) || (R898_INFO.RF_KHz>3400000))
			return RT_Fail;

		if(R898_S_PLL(R898_Tuner_Num,R898_INFO.RF_KHz)!=RT_Success)
		 return RT_Fail;

//#if (QUICKLY_PLL_LOCK == TRUE)
#if 0
		Sleep(1);
#else
		//16MHz : atune = 62.5kHz  => 62.5kHz / 8(clock) / 63(bank) = 8.06ms
		//24MHz : atune = 93.75kHz => 93.75kHz / 8(clock) / 63(bank) = 5.37ms
		//27MHz : atune = 105kHz => 105kHz / 8(clock) / 63(bank) = 4.8ms
		Sleep(10);
#endif

		/*R898_I2C.RegAddr = 10;	
		if(R898_INFO.RF_KHz >= 3100000) //set Mixer LO Buf vdd: High  R10[6:5] = 10
		{
			R898_S_Array[R898_I2C.RegAddr] &= 0x9F;
			R898_S_Array[R898_I2C.RegAddr] = (R898_S_Array[R898_I2C.RegAddr] | 0x40);
		}
		else if(R898_INFO.RF_KHz >= 2700000) //set Mixer LO Buf vdd: Low  R10[6:5] = 01
		{
			R898_S_Array[R898_I2C.RegAddr] &= 0x9F;
			R898_S_Array[R898_I2C.RegAddr] = (R898_S_Array[R898_I2C.RegAddr] | 0x20);
		}
		else  //Lowest R10[6:5] = 00
		{
			R898_S_Array[R898_I2C.RegAddr] &= 0x9F;
		}

		R898_I2C.Data = R898_S_Array[R898_I2C.RegAddr];
		if(I2C_Write(TUNER_NO,&R898_I2C) != RT_Success)
			return RT_Fail;*/



#if(R898_S_FOR_SHARP==TRUE)
		if(R898_INFO.RF_KHz>=R898_INFO.GpioFrequency_KHz)
#else
		if(R898_INFO.RF_KHz<=R898_INFO.GpioFrequency_KHz)
#endif
		{
			R898_S_Array[9] |= 0x20;  //set gpio1 R9[5]=1
			R898_S_Array[22] &= 0xF7; //set gpio  R22[3]=0

		}
		else
		{
			R898_S_Array[9] &= 0xDF;  //set gpio1 R9[5]=0
			R898_S_Array[22] |= 0x08; //set gpio R22[3]=1
		}

		R898_I2C.RegAddr = 9;	
		R898_I2C.Data = R898_S_Array[R898_I2C.RegAddr];
		if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
			return RT_Fail;

		R898_I2C.RegAddr = 22;	
		R898_I2C.Data = R898_S_Array[R898_I2C.RegAddr];
		if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
			return RT_Fail;

	
	
		R898_INFO.SymbolRate_Kbps -= 1000;

		switch (R898_INFO.R898_SATELLITE_RollOff_Mode)
		{
		case ROLL_OFF_0_03:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 103)/10); 
			break;
		case ROLL_OFF_0_10:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 110)/10);
			break;
		case ROLL_OFF_0_15:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 115)/10); 
			break;
		case ROLL_OFF_0_20:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 120)/10);
			break;
		case ROLL_OFF_0_25:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 125)/10);
			break;
		case ROLL_OFF_0_30:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 130)/10);
			break;
		case ROLL_OFF_0_35:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 135)/10);
			break;
		case ROLL_OFF_0_40:
			R898_satellite_bw = (UINT32)((R898_INFO.SymbolRate_Kbps * 140)/10);
			break;
		}

		if(R898_satellite_bw == R898_pre_satellite_bw[R898_Tuner_Num])
		{
			//no change register 
		}
		else
		{
			do    //0~58
			{
				if(R898_satellite_bw <= R898_S_BandWidth[fil_cnt])
				{
					//Reg13[5:0]
					R898_I2C.RegAddr = 13;
					R898_S_Array[R898_I2C.RegAddr] &= 0xC0;
					R898_S_Array[R898_I2C.RegAddr] |= (R898_S_Coras_tune[fil_cnt]+1);
					R898_I2C.Data = R898_S_Array[R898_I2C.RegAddr];
					if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
						return RT_Fail;
					//Reg12[3:2]
					R898_I2C.RegAddr = 12;
					R898_S_Array[R898_I2C.RegAddr] &= 0xF3;
					R898_S_Array[R898_I2C.RegAddr] |= (R898_S_Fine_tune[fil_cnt]<<2);
					R898_I2C.Data = R898_S_Array[R898_I2C.RegAddr];
					if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
						return RT_Fail;
					break;
				}
				fil_cnt += 1;
			}while(fil_cnt < 59);

			R898_pre_satellite_bw[R898_Tuner_Num] = R898_satellite_bw;
		}

		for(int icount=0;icount<4;icount++)
		{
			R898_S_R1TOR4_Write_Arry[icount]=R898_S_Array[icount+1];
		}
		R898_S_SBY_FLAG[R898_Tuner_Num] = FALSE;

		return RT_Success;
}






R898_ErrCode R898_SetPllData(R898_TUNER_NUM R898_Tuner_Num, R898_Set_Info R898_INFO)
{
	if(R898_Initial_done_flag[R898_Tuner_Num]==FALSE)
	{
		R898_Init(R898_Tuner_Num);
	}
	else
	{
		 if(R898_T_SBY_FLAG[R898_Tuner_Num] == TRUE)
			 R898_T_Wakeup(R898_Tuner_Num);
	}

	if(R898_INFO.R898_Standard!=R898_SATELLITE)
	{
      if(R898_SetStandard(R898_Tuner_Num, R898_INFO.R898_Standard) != RT_Success)
	  {
		  return RT_Fail;
	  }
      if(R898_SetFrequency(R898_Tuner_Num, R898_INFO) != RT_Success)
	  {
          return RT_Fail;
	  }
	  R898_SATELLITE_FLAG[R898_Tuner_Num] = 0;
	}
	else
	{
		if(R898_Satellite_Setting(R898_Tuner_Num, R898_INFO) != RT_Success)
			return RT_Fail;
		R898_SATELLITE_FLAG[R898_Tuner_Num] = 1;
	}

	if(R898_INFO.R898_Standard!=R898_SATELLITE)
	{
		 if(R898_S_Standby(R898_Tuner_Num) != RT_Success)
			  return RT_Fail;
	}
	else
	{
		 if(R898_T_Standby(R898_Tuner_Num) != RT_Success)
			  return RT_Fail;
	}


      return RT_Success;
}



R898_ErrCode R898_S_PLL(R898_TUNER_NUM R898_Tuner_Num,UINT32 Freq)
{
	UINT8  MixDiv   = 2;

	UINT8  Ni       = 0;
	UINT8  Si       = 0;
	UINT8  DivNum   = 0;
	UINT8  Nint     = 0;
	UINT32 VCO_Min  = 3400000;
	/*if (Freq>=3250000)
	{
		VCO_Min  = Freq;
	}*/
	UINT32 VCO_Max  = 3400000*2;
	UINT32 VCO_Freq = 0;
	UINT32 PLL_Ref	= R898_Xtal;	
	UINT32 VCO_Fra	= 0;		//VCO contribution by SDM (kHz)
	UINT16 Nsdm		= 2;
	UINT16 SDM		= 0;
	UINT16 SDM16to9	= 0;
	UINT16 SDM8to1  = 0;
	//UINT8  Judge    = 0;


//#if (QUICKLY_PLL_LOCK == TRUE)
#if 0
	UINT32 VCO_Freq_List[8] = {2000000, 2400000, 2800000, 2950000, 3600000, 3900000, 4200000, 5300000};
	UINT8 VCO_Bank_List[7] = {4, 12, 20, 27, 45, 52, 59};
	UINT8 VCO_Index = 0;
	UINT8 VCO_Manual_Bank = 0;
	UINT8 store_reg3_value = 0;
#endif

	while(MixDiv <= 32)  // 2 -> 4 -> 8 -> 16
	{
		if(((Freq * MixDiv) >= VCO_Min) && ((Freq * MixDiv) <= VCO_Max))// Lo_Freq->Freq
		{
			if(MixDiv==2)
				DivNum = 0;
			else if(MixDiv==4)
				DivNum = 1;
			else if(MixDiv==8)
				DivNum = 2;
			else if(MixDiv==16)
				DivNum = 3;
			else   //16
				DivNum = 4;
			break;
		}
		MixDiv = MixDiv << 1;
	}

	//Divider R17[4:3]
	R898_I2C.RegAddr = 0x11;
	R898_S_Array[17] &= 0xC7;
	R898_S_Array[17] |= (DivNum<<3) ;
	R898_I2C.Data = R898_S_Array[17];
	if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
		return RT_Fail;



	VCO_Freq = Freq * MixDiv;		// Lo_Freq->Freq

//#if (QUICKLY_PLL_LOCK == TRUE)
#if 0
	//Check VCO frequency list and manual bank, let the R710 PLL circuit lock quickly. 
	//UINT16 VCO_Freq_List[8] = {2000, 2400, 2800, 2950, 3600, 3900, 4200, 5300};
	//UINT8 VCO_Bank_List[8] = {4, 12, 20, 27, 45, 52, 59};
	//UINT8 VCO_Index = 0;
	//UINT8 VCO_Manual_Bank = 0;
	do
	{
		if(((VCO_Freq > VCO_Freq_List[VCO_Index]) && (VCO_Freq <= VCO_Freq_List[VCO_Index+1])))
		{
			VCO_Manual_Bank = VCO_Bank_List[VCO_Index];
			break;
		}
		else
		{
			VCO_Index += 1;
		}
	}while(VCO_Index<7);

	if(VCO_Index==7)
	{
		VCO_Manual_Bank = 27;
	}

	store_reg3_value = R898_S_Array[3];
	R898_I2C.RegAddr = 0x03;
	R898_S_Array[3] &= 0x80;
	R898_S_Array[3] |= 0x40; //set vco manual mode R3[6]=1   {0: auto, 1: manual}
	R898_S_Array[3] |= VCO_Manual_Bank; //set manual bank R3[5:0]
	R898_I2C.Data    = R898_S_Array[3];
	if(I2C_Write(TUNER_NO,&R898_I2C) != RT_Success)
		return RT_Fail;

#endif	

	Nint     = (UINT8) (VCO_Freq / 2 / PLL_Ref);
	VCO_Fra  = (UINT16) (VCO_Freq - 2 * PLL_Ref * Nint);

	//boundary spur prevention
	if (VCO_Fra < PLL_Ref/64)             // 2*PLL_Ref/128
		VCO_Fra = 0;
	else if (VCO_Fra > PLL_Ref*127/64)    // 2*PLL_Ref*127/128
	{
		VCO_Fra = 0;
		Nint ++;
	}
	else if((VCO_Fra > PLL_Ref*127/128) && (VCO_Fra < PLL_Ref)) //> 2*PLL_Ref*127/256,  < 2*PLL_Ref*128/256
		VCO_Fra = PLL_Ref*127/128;      // VCO_Fra = 2*PLL_Ref*127/256
	else if((VCO_Fra > PLL_Ref) && (VCO_Fra < PLL_Ref*129/128)) //> 2*PLL_Ref*128/256,  < 2*PLL_Ref*129/256
		VCO_Fra = PLL_Ref*129/128;      // VCO_Fra = 2*PLL_Ref*129/256
	else
		VCO_Fra = VCO_Fra;

	//N & S	
	Ni       = (Nint - 13) / 4;
	Si       = Nint - 4 *Ni - 13;
	R898_I2C.RegAddr = 0x11;
	R898_S_Array[17]  &= 0x3F;
	R898_S_Array[17] |= (Si << 6);
	R898_I2C.Data  = R898_S_Array[17];
	if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 0x12;
	R898_S_Array[18] &= 0xC0;
	R898_S_Array[18] |= Ni;
	R898_I2C.Data  = R898_S_Array[18];
	if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
		return RT_Fail;

	//Dither R17[5],
	R898_I2C.RegAddr = 0x03;
	R898_S_Array[3] &= 0xF7;
	if(VCO_Fra == 0)
		R898_S_Array[3] |= 0x08;
	R898_I2C.Data = R898_S_Array[3];
	if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
		return RT_Fail;

	//SDM R18[6]
	R898_I2C.RegAddr = 0x12;
	R898_S_Array[18] &= 0xBF;
	if(VCO_Fra == 0)
		R898_S_Array[18] |= 0x40;
	R898_I2C.Data = R898_S_Array[18];
	if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
		return RT_Fail;

	//SDM calculator
	while(VCO_Fra > 1)
	{			
		if (VCO_Fra > (2*PLL_Ref / Nsdm))
		{		
			SDM = SDM + 32768 / (Nsdm/2);
			VCO_Fra = VCO_Fra - 2*PLL_Ref / Nsdm;
			if (Nsdm >= 0x8000)
				break;
		}
		Nsdm = Nsdm << 1;
	}

	SDM16to9 = SDM >> 8;
	SDM8to1 =  SDM - (SDM16to9 << 8);

	R898_I2C.RegAddr = 0x14;
	R898_S_Array[20]   = (UINT8) SDM16to9;
	R898_I2C.Data    = R898_S_Array[20];
	if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
		return RT_Fail;

	R898_I2C.RegAddr = 0x13;
	R898_S_Array[19]   = (UINT8) SDM8to1;
	R898_I2C.Data    = R898_S_Array[19];
	if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
		return RT_Fail;

//#if (QUICKLY_PLL_LOCK == TRUE)
#if 0
	//Restore the register original settings
	R898_I2C.RegAddr = 0x03;
	R898_S_Array[3] = store_reg3_value;
	R898_I2C.Data    = R898_S_Array[3];
	if(I2C_Write(TUNER_NO,&R898_I2C) != RT_Success)
		return RT_Fail;
#endif
    return RT_Success;
}

R898_ErrCode R898_S_GetLNA_RF_MIXER_Gain(R898_TUNER_NUM R898_Tuner_Num,UINT8 *agc_slope_flag ,UINT8 *if_adc,R898_S_LNA_Gain_Info *R898_S_lna_gain,R898_S_RF_Gain_Info *R898_S_rf_gain,R898_S_MIXER_Gain_Info *R898_S_mixer_gain,UINT32 RF_Freq_Khz)
{

	UINT8  lna_gain_read_value;
	UINT8  rf_gain_read_value;
//	UINT8  if_adc_value;

	//I2C_LEN_TYPE Dlg_I2C_Len;
	//Dlg_I2C_Len.RegAddr = 0x00;
	//Dlg_I2C_Len.Len     = 0x0E;


	
	R898_I2C_Len.RegAddr = 0x00;
	R898_I2C_Len.Len =0x0E;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_SATELITE];
	if(I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
	{
		I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len);
	}


	//I2C_Read_Len(RT740_Tuner_Num,&Dlg_I2C_Len) ; // read data length

	//if_adc_value = (Dlg_I2C_Len.Data[1] & 0x7F); //get if adc value

	*if_adc = (R898_I2C_Len.Data[1] & 0x7F); //get if adc value
	*agc_slope_flag = (R898_I2C_Len.Data[13] & 0x40)>>6; //get if adc value

	lna_gain_read_value = (R898_I2C_Len.Data[2] & 0x1F); //get lna gain
	rf_gain_read_value = (R898_I2C_Len.Data[4] & 0x0F); //get rf gain


	/*if(RF_Freq_Khz<1999000)
	{
		RT740_lna_gain->LNA_gain = RT740_LNA_Index_Under2G[lna_gain_read_value];
		RT740_rf_gain->RF_gain = RT740_RF_Index_Under2G[lna_gain_read_value];
		RT740_mixer_gain->MIXER_gain = RT740_Mixer_Index_Under2G[lna_gain_read_value];
	}
	else
	{
		RT740_lna_gain->LNA_gain = RT740_LNA_Index_Over2G[lna_gain_read_value];
		RT740_rf_gain->RF_gain = RT740_RF_Index_Over2G[rf_gain_read_value];
		RT740_mixer_gain->MIXER_gain = RT740_Mixer_Index_Over2G[rf_gain_read_value];
	}*/


	R898_S_lna_gain->LNA_gain = R898_S_LNA_Index[lna_gain_read_value];
	R898_S_rf_gain->RF_gain = R898_S_RF_Index[lna_gain_read_value];
	R898_S_mixer_gain->MIXER_gain = R898_S_Mixer_Index[lna_gain_read_value];
	
	
	
	
	return RT_Success;
}
R898_ErrCode R898_T_Standby(R898_TUNER_NUM R898_Tuner_Num)
{
	UINT8 InitArrayCunt = 0;
	UINT8 XtalCap, CapTot;

	R898_I2C_Len.RegAddr = 0x00;   //  R898_T:0x10
	R898_I2C_Len.Len = R898_T_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];
	
	UINT8 R898_T_Standby_Reg_Arry[R898_T_REG_NUM] = { //R16-R63
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F
		0xF9, 0x8F, 0x7F, 0xFF, 0x5C, 0x00, 0x01, 0x88, 0x00, 0x00,
	//  0x10  0x11  0x12  0x13  0x14  0x15  0x16  0x17  0x18  0x19
		0x00, 0x40, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x3C, 0x42, 0x80, 
	//  0x1A  0x1B  0x1C  0x1D  0x1E  0x1F  0x20  0x21  0x22  0x23   
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x8C,
	//  0x24  0x25  0x26  0x27  0x28  0x29  0x2A  0x2B  0x2C  0x2D
		0xCA, 0x00, 0x44, 0x44, 0x88, 0x38, 0xE6, 0x0C, 0xFC, 0x00,
	//  0x2E  0x2F  0x30  0x31  0x32  0x33  0x34  0x35  0x36  0x37
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20			 };
	//  0x38  0x39  0x3A  0x3B  0x3C  0x3D  0x3E  0x3F  

	R898_T_Standby_Reg_Arry[20] = R898_T_Array[20];
	R898_T_Standby_Reg_Arry[22] = R898_T_Array[22];
	if(R898_S_SBY_FLAG[R898_Tuner_Num] ==1)
	{
		R898_T_Standby_Reg_Arry[16] = R898_T_Standby_Reg_Arry[16] | 0x06;
		R898_T_Standby_Reg_Arry[17] = R898_T_Standby_Reg_Arry[17] | 0x60;
		R898_T_Standby_Reg_Arry[20] = R898_T_Standby_Reg_Arry[20] & 0xEF;
	}

	if(R898_XTAL_CAP>30)
	{
		CapTot = R898_XTAL_CAP-10;
		XtalCap = 1;  //10
	}
	else
	{
		CapTot = R898_XTAL_CAP;
		XtalCap = 0;  //0
	}

	R898_T_Standby_Reg_Arry[33]=(R898_T_Standby_Reg_Arry[33] & 0xC0) |  (XtalCap<<5) | CapTot;


	for(InitArrayCunt = 0; InitArrayCunt<R898_T_REG_NUM; InitArrayCunt ++)
	{
		
		
		R898_T_SBY[R898_Tuner_Num][InitArrayCunt] = R898_T_Array[InitArrayCunt];
		R898_I2C_Len.Data[InitArrayCunt] = R898_T_Standby_Reg_Arry[InitArrayCunt];
		R898_T_Array[InitArrayCunt] = R898_T_Standby_Reg_Arry[InitArrayCunt];
		
	}

	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;
	if(R898_S_SBY_FLAG[R898_Tuner_Num] ==1)
	{
		R898_I2C.I2cAddr=R898_ADDRESS[R898_SATELITE];
		R898_S_Array[6] = R898_S_Array[6] | 0x20;
		R898_I2C.RegAddr = 6;	
		R898_I2C.Data = R898_S_Array[R898_I2C.RegAddr];
		if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
			return RT_Fail;
	}

	R898_T_SBY_FLAG[R898_Tuner_Num] = TRUE;

	//R898_T_SBY[MAX_TUNER_NUM][R898_T_REG_NUM]

	
	return RT_Success;
}

R898_ErrCode R898_S_Standby(R898_TUNER_NUM R898_Tuner_Num)
{
	UINT8 InitArrayCunt = 0;

	R898_I2C_Len.RegAddr = 0x00;   //  R898_S:0x00
	R898_I2C_Len.Len = R898_S_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_SATELITE];
	
	
	UINT8 R898_S_Standby_Reg_Arry[R898_S_REG_NUM] = {0x00, 0x00, 0x00, 0x40, 0xCF, 0xFF, 0x0F, 0x1D, 0xDD, 0x1C, 
									   0xC5, 0x67, 0xE9, 0x0F, 0xE0, 0x0C, 0x80, 0x88, 0x11, 0x55,
									   0x55, 0x00, 0x10, 0x6C, 0x0D, 0x11, 0xD3, 0x00, 0xC8, 0x19,
									   0x4A, 0x17};

	if(R898_T_SBY_FLAG[R898_Tuner_Num] == 1)
		R898_S_Standby_Reg_Arry[6] = R898_S_Standby_Reg_Arry[6] | 0x20;

	for(InitArrayCunt = 0; InitArrayCunt<R898_S_REG_NUM; InitArrayCunt ++)
	{
		R898_I2C_Len.Data[InitArrayCunt] = R898_S_Standby_Reg_Arry[InitArrayCunt];
		R898_S_Array[InitArrayCunt] = R898_S_Standby_Reg_Arry[InitArrayCunt];
	}


	
	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;

	if(R898_T_SBY_FLAG[R898_Tuner_Num] ==1)
	{
		R898_I2C.I2cAddr=R898_ADDRESS[R898_TUNER];
		R898_T_Array[16] = 0xFF;
		R898_T_Array[17] = 0xEF;
		R898_T_Array[20] = R898_T_Array[20] & 0xCF;
		R898_I2C.RegAddr = 16;	
		R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
		if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
			return RT_Fail;
		R898_I2C.RegAddr = 17;	
		R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
		if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
			return RT_Fail;
		R898_I2C.RegAddr = 20;	
		R898_I2C.Data = R898_T_Array[R898_I2C.RegAddr];
		if(I2C_Write(R898_Tuner_Num,&R898_I2C) != RT_Success)
			return RT_Fail;
	}
	R898_S_Initial_done_flag[R898_Tuner_Num] = FALSE;


	R898_S_SBY_FLAG[R898_Tuner_Num] = TRUE;


	
	return RT_Success;

}
R898_ErrCode R898_T_Wakeup(R898_TUNER_NUM R898_Tuner_Num)
{
	UINT8 InitArrayCunt = 0;

	R898_I2C_Len.RegAddr = 0x00;   //  R898_T:0x10
	R898_I2C_Len.Len = R898_T_REG_NUM;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];


	for(InitArrayCunt = 0; InitArrayCunt<R898_T_REG_NUM; InitArrayCunt ++)
	{
	

		R898_I2C_Len.Data[InitArrayCunt] = R898_T_SBY[R898_Tuner_Num][InitArrayCunt];
		R898_T_Array[InitArrayCunt] = R898_T_SBY[R898_Tuner_Num][InitArrayCunt];
		
	}

	if(I2C_Write_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
		return RT_Fail;

	R898_T_SBY_FLAG[R898_Tuner_Num] = FALSE;

	
	return RT_Success;


	


}	
R898_ErrCode R898_GetRfGain(R898_TUNER_NUM R898_Tuner_Num, R898_RF_Gain_Info *pR898_rf_gain)
{

	R898_I2C_Len.RegAddr = 0x00;
	R898_I2C_Len.Len =0x05;
	R898_I2C_Len.I2cAddr=R898_ADDRESS[R898_TUNER];

	if(I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len) != RT_Success)
	{
		I2C_Read_Len(R898_Tuner_Num, &R898_I2C_Len);
	}
	pR898_rf_gain->RF_gain1 = (R898_I2C_Len.Data[3] & 0x1F);            //lna    //R898 R3[4:0]  
	pR898_rf_gain->RF_gain2 = ((R898_I2C_Len.Data[4] & 0x0F));			 //rf	 //R898 R4[3:0]  
	pR898_rf_gain->RF_gain3 = ((R898_I2C_Len.Data[4] & 0xF0)>>4);       //mixer  //R898 R4[7:4]  	

	pR898_rf_gain->RF_gain4 = ((R898_I2C_Len.Data[2] & 0xC0)>>6 );       //filter  //R898 R2[7:6]  +	R3[7:6]
	pR898_rf_gain->RF_gain4 = pR898_rf_gain->RF_gain4 + ((R898_I2C_Len.Data[3] & 0xC0)>>6) *4;      


    return RT_Success;
}


int poes()
{

    return 0;
}