//#include "..\stdafx.h"
#ifndef  _R898_H_ 
#define _R898_H_
#include "extra.h"
//----------------------------------------------------------//
//                   Define                                 //
//									//
//----------------------------------------------------------//
/*
Internal xtal cap is 10pF for all tuner.

If define R898_MULTI_TUNER_APPLICATION TRUE 
CHIP_1 tuner xtal driver current is (High,Highest).
Other tuner xtal driver current is (High,Lowest).

If define R898_MULTI_TUNER_APPLICATION FALSE
Do Xtal check function
*/
#define FOR_KOREA_CTMR  FALSE
#define FOR_TDA10024    0

#define VERSION   "R898_FM_GUI_v1.3i"


#define R898_T_REG_NUM         64
#define R898_S_REG_NUM         32


#define R898_TF_HIGH_NUM      8
#define R898_TF_MID_NUM       11
#define R898_TF_LOW_NUM       9


#define R898_TF_LOWEST_NUM   8
#define R898_RING_POWER_FREQ_LOW  115000
#define R898_RING_POWER_FREQ_HIGH 450000
#define R898_IMR_IF              5300
#define R898_IMR_TRIAL       9

//----------------------------------------------------------//
//                   Internal Structure                     //
//----------------------------------------------------------//

typedef struct _Sys_Info_Type
{
	UINT16		   IF_KHz;
	UINT16		   FILT_CAL_IF;
	UINT8          BW;
	UINT8		   V17M;
	UINT8		   HPF_COR_R36_0_4BT;
	UINT8          LNA_DET_R_R54_2_2BT;
	UINT8          FILT_EXT_ENA;
	UINT8          FILT_EXT_WIDEST;
	UINT8          FILT_EXT_POINT;
	UINT8		   FILT_COMP;
	UINT8		   FILT_CUR;  
	UINT8		   FILT_3DB; 
	UINT8		   SWBUF_CUR;  
	UINT8          TF_CUR;              
	UINT8		   INDUC_BIAS;  
	UINT8          SWCAP_CLK;
	UINT8		   NA_PWR_DET;  
}Sys_Info_Type;

typedef struct _Freq_Info_Type
{
	UINT8		RF_POLY;
	UINT8		LNA_BAND;
	UINT8		LPF_CAP;
	UINT8		LPF_NOTCH;
    UINT8		XTAL_POW0;
	UINT8		CP_CUR;
	UINT8		IMR_MEM;
	UINT8		Q_CTRL;   
	UINT8       BYP_LPF; 
}Freq_Info_Type;

typedef struct _SysFreq_Info_Type
{
	UINT8		QCTRL_29_R21_3_1BT;
	UINT8		LNA_TF_QCTRL_R21_2_1BT;
	UINT8		LNA_MAX_GAIN_R20_0_2BT;
	UINT8		LNA_MIN_GAIN_R21_0_1BT;
	UINT8		LNA_TOP_R51_0_3BT;
	UINT8		EXTRA_LNA_TOP_R51_3_1BT;
	UINT8		LNA_VTH_R19_3_4BT;
	UINT8		LNA_VTL_R48_4_4BT;
	UINT8		RF_TOP_R51_4_3BT;
	UINT8		RF_VTH_R24_1_4BT;
	UINT8		RF_VTL_R49_4_4BT;
	UINT8		NRB_TOP_R52_0_4BT;
	UINT8		NRB_BW_LPF_R52_4_2BT;
	UINT8		NRB_DET_HPF_R38_2_2BT;
	UINT8		MIXER_TOP_R54_1_3BT;
	UINT8		MIXER_VTH_R25_4_4BT;
	UINT8		MIXER_VTL_R50_4_4BT;
	UINT8		FILTER_TOP_R29_6_2BT;
	UINT8		FILTER_VTH_R36_4_4BT;
	UINT8		FILTER_VTL_R38_4_4BT;
	UINT8		MIXG_LIMIT_R48_0_3BT;
	UINT8       LIMITG_IMG_ADD_R27_4_2BT;
	UINT8		MIXER_AMP_LPF_R26_5_3BT;
	UINT8		MIXER_LPF_ADD_R44_2_2BT;
	UINT8		RF_BUF_LPF_R49_0_2BT;// new
	UINT8		RBG_MIN_RFBUF_R25_0_1BT;
	UINT8		RBG_MAX_RFBUF_R24_7_1BT;
	UINT8		LNA_DIS_MODE_R53_5_1BT;
	UINT8		RF_DIS_MODE_R53_6_1BT;
	UINT8		LNA_DIS_CURR_R54_0_1BT;
	UINT8		RF_DIS_CURR_R53_0_1BT;
	UINT8		LNA_RF_DIS_SLOW_R55_0_2BT;
	UINT8		LNA_RF_DIS_FAST_R55_2_2BT;
	UINT8		SLOW_DIS_ENABLE_R56_1_1BT;
	UINT8		BB_DIS_CURR_R55_6_1BT;
	UINT8		MIXER_FILTER_DIS_R55_4_2BT;
	UINT8		FAGC_2_SAGC_R56_4_1BT;
	UINT8		CLK_FAST_R58_6_2BT;
	UINT8		CLK_SLOW_R58_4_2BT;
	UINT8		LEVEL_SW_R57_4_1BT;
	UINT8		ATV_MODE_SEL_R57_7_1BT;
	UINT8		LEVEL_SW_VTHH_R57_5_1BT;
	UINT8		LEVEL_SW_VTLL_R57_6_1BT;
	UINT8		FILTER_G_CONTROL_R47_1_1BT;
	UINT8		RF_WIDEN_VTH_VTL_R31_6_2BT;
	UINT8		BB_WIDEN_VTH_VTL_R47_4_2BT;
	UINT8		IMG_NRB_ADDER_R17_2_2BT;
	UINT8       LPF_3TH_HGAIN_R28_7_1BT;
	UINT8		LPF_3TH_GAIN_R37_6_2BT;
	UINT8		VGA_INR_R26_4_1BT;
	UINT8		VGA_INC_R47_5_2BT;
	UINT8		ATTUN_VGA_R31_3_1BT;
	UINT8		VGA_SHIFTGAIN_R31_2_1BT;
	UINT8		VGA_ATT5DB_R31_4_1BT;
}SysFreq_Info_Type;
typedef enum _R898_VTH_Type  
{
	R898_VTH_0_17=0,
	R898_VTH_0_28,
	R898_VTH_0_39,
	R898_VTH_0_50,
	R898_VTH_0_61,
	R898_VTH_0_72,
	R898_VTH_0_83,
	R898_VTH_0_94,
	R898_VTH_0_05,
	R898_VTH_1_16,
	R898_VTH_1_27,
	R898_VTH_1_38,
	R898_VTH_1_49,
	R898_VTH_1_60,
	R898_VTH_1_71,
	R898_VTH_1_82,
}R898_VTH_Type;
typedef enum _R898_VTL_Type  
{
	R898_VTL_0_47=0,
	R898_VTL_0_58,
	R898_VTL_0_69,
	R898_VTL_0_80,
	R898_VTL_0_91,
	R898_VTL_1_02,
	R898_VTL_1_13,
	R898_VTL_1_24,
	R898_VTL_1_35,
	R898_VTL_1_46,
	R898_VTL_1_57,
	R898_VTL_1_68,
	R898_VTL_1_79,
	R898_VTL_1_90,
	R898_VTL_2_01,
	R898_VTL_2_12,
}R898_VTL_Type;

typedef struct _R898_Cal_Info_Type
{
	UINT8		FILTER_6DB;
	UINT8		MIXER_AMP_GAIN;
	UINT8		MIXER_BUFFER_GAIN;
	UINT8		LNA_GAIN;
	UINT8		LNA_POWER;
	UINT8		RFBUF_OUT;
	UINT8		RFBUF_POWER;
	UINT8		TF_CAL;
}R898_Cal_Info_Type;

typedef struct _R898_SectType
{
	UINT8   Phase_Y;
	UINT8   Gain_X;
	UINT8   Iqcap;
	UINT8   Value;
}R898_SectType;

typedef struct _R898_TF_Result
{
	UINT8   TF_Set;
	UINT8   TF_Value;
}R898_TF_Result;

typedef enum _R898_TF_Type
{	
	R898_TF_270N_39N = 0,  //270n/39n   (DTV Terr) 
	R898_TF_BEAD_68N,      //Bead/68n   (China DVB-C, DTMB, ATV) 
	R898_TF_270N_27N,
	R898_TF_BEAD_27N,
	R898_TF_390N_27N,
	R898_TF_390N_39N,        //only for LGIT
	R898_TF_SIZE
}R898_TF_Type;

typedef enum _R898_UL_TF_Type
{
	R898_UL_USING_BEAD = 0,            
    R898_UL_USING_270NH,                      
	R898_UL_USING_390NH,  
}R898_UL_TF_Type;

typedef enum _R898_MID_TF_Type
{
	R898_MID_USING_27NH = 0,            
	R898_MID_USING_39NH,      //39n
	R898_MID_USING_68NH,      //68n
}R898_MID_TF_Type;


typedef enum _TUNER_NUM
{
	R898_TUNER_1 = 0,   //master
	R898_TUNER_2,
	R898_TUNER_3,
	R898_TUNER_4,
	R898_TUNER_5,
	R898_TUNER_6,
	MAX_TUNER_NUM
}R898_TUNER_NUM;

typedef enum _R898_TF_Band_Type
{
    TF_HIGH = 0,
	TF_MID,
	TF_LOW
}R898_TF_Band_Type;

typedef enum _R898_Cal_Type
{
	R898_IMR_CAL = 0,
	R898_IMR_LNA_CAL,
	R898_TF_CAL,
	R898_TF_LNA_CAL,
	R898_LPF_CAL,
	R898_LPF_LNA_CAL
}R898_Cal_Type;

typedef enum _R898_BW_Type
{
	BW_6M = 0,
	BW_7M,
	BW_8M,
	BW_1_7M,
	BW_10M,
	BW_200K
}R898_BW_Type;


enum XTAL_PWR_VALUE
{
	XTAL_SMALL_LOWEST = 0,
    XTAL_SMALL_LOW,
    XTAL_SMALL_HIGH,
    XTAL_SMALL_HIGHEST,
    XTAL_LARGE_HIGHEST,
	XTAL_LARGE_STRONG,
	XTAL_CHECK_SIZE
};


typedef enum _R898_Xtal_Div_TYPE
{
	XTAL_DIV1 = 0,
	XTAL_DIV2
}R898_Xtal_Div_TYPE;

//----------------------------------------------------------//
//                   Type Define                                    //
//----------------------------------------------------------//
#define UINT8  unsigned char
#define UINT16 unsigned short
#define UINT32 unsigned long

//----------------------------------------------------------//
//                   R898 Public Parameter                     //
//----------------------------------------------------------//
typedef enum _R898_ErrCode
{
	RT_Success = true,
	RT_Fail    = false
}R898_ErrCode;

typedef enum _R898_Standard_Type  //Don't remove standand list!!
{
	R898_MN_5100 = 0,          //for NTSC_MN, PAL_M (IF=5.1M)
	R898_MN_5800,              //for NTSC_MN, PLA_M (IF=5.8M)
	R898_PAL_I,                //for PAL-I
	R898_PAL_DK,               //for PAL DK in non-"DTMB+PAL DK" case
	R898_PAL_B_7M,             //no use
	R898_PAL_BGH_8M,           //for PAL B/G, PAL G/H
	R898_SECAM_L,              //for SECAM L
	R898_SECAM_L1,             //for SECAM L'
	R898_SECAM_L1_INV,       
	R898_MN_CIF_5M,             //for NTSC_MN, PLA_M (CIF=5.0M)
	R898_PAL_I_CIF_5M,          //for PAL-I (CIF=5.0M)
	R898_PAL_DK_CIF_5M,         //for PAL DK (CIF=5M)
	R898_PAL_B_7M_CIF_5M,       //for PAL-B 7M (CIF=5M)
	R898_PAL_BGH_8M_CIF_5M,     //for PAL G/H 8M (CIF=5M)
	R898_SECAM_L_CIF_5M,        //for SECAM L (CIF=5M)
	R898_SECAM_L1_CIF_5M,       //for SECAM L' (CIF=5M)
	R898_SECAM_L1_INV_CIF_5M,   //(CIF=5M)
	R898_ATV_SIZE,
	R898_DVB_T_6M = R898_ATV_SIZE,
	R898_DVB_T_7M,
	R898_DVB_T_8M, 
    R898_DVB_T2_6M,       //IF=4.57M
	R898_DVB_T2_7M,       //IF=4.57M
	R898_DVB_T2_8M,       //IF=4.57M
	R898_DVB_T2_1_7M,
	R898_DVB_T2_10M,
	R898_DVB_C_8M,
	R898_DVB_C_6M, 
	R898_J83B,
	R898_ISDB_T,         //IF=4.063M
	R898_ISDB_T_4570,		 //IF=4.57M
	R898_DTMB_4570,      //IF=4.57M
	R898_DTMB_6000,     //IF=6.00M
	R898_DTMB_6M_BW_IF_5M,   //IF=5.0M, BW=6M
	R898_DTMB_6M_BW_IF_4500, //IF=4.5M, BW=6M
	R898_ATSC,
	R898_SATELLITE,
	R898_DVB_T_6M_IF_5M,
	R898_DVB_T_7M_IF_5M,
	R898_DVB_T_8M_IF_5M,
	R898_DVB_T2_6M_IF_5M,
	R898_DVB_T2_7M_IF_5M,
	R898_DVB_T2_8M_IF_5M,
	R898_DVB_T2_1_7M_IF_5M,
	R898_DVB_C_8M_IF_5M,
	R898_DVB_C_6M_IF_5M, 
	R898_J83B_IF_5M,
	R898_ISDB_T_IF_5M,//47            
	R898_ISDB_C_IF_5M,//48
	R898_DTMB_IF_5M,     
	R898_ATSC_IF_5M,  //50

	R898_STD_SIZE,
}R898_Standard_Type;


typedef enum _R898_GPO_Type
{
	HI_SIG = TRUE,
	LO_SIG = FALSE
}R898_GPO_Type;

typedef enum _R898_RF_Gain_TYPE
{
	RF_AUTO = 0,
	RF_MANUAL
}R898_RF_Gain_TYPE;

typedef enum _R898_Xtal_Cap_TYPE
{
	XTAL_CAP_LARGE = 0,
	XTAL_CAP_SMALL
}R898_Xtal_Cap_TYPE;

typedef enum _R898_SATELLITE_OutputSignal_Type
{
	DIFFERENTIALOUT = TRUE,
	SINGLEOUT     = FALSE
}R898_SATELLITE_OutputSignal_Type;

typedef enum _R898_SATELLITE_AGC_Type
{
	AGC_NEGATIVE = TRUE,
	AGC_POSITIVE = FALSE
}R898_SATELLITE_AGC_Type;

typedef enum _R898_SATELLITE_AttenVga_Type
{
	ATTENVGAON = TRUE,
	ATTENVGAOFF= FALSE
}R898_SATELLITE_AttenVga_Type;

typedef enum _R898_SATELLITE_FineGain_Type
{
	FINEGAIN_3DB = 0,
	FINEGAIN_2DB,
	FINEGAIN_1DB,
	FINEGAIN_0DB
}R898_SATELLITE_FineGain_Type;

typedef enum _R898_IntTunerNum_Type  //R858 Internal tuner num
{
	R898_TUNER = 0,
	R898_SATELITE
}R898_IntTunerNum_Type;


typedef enum _R898_SATELLITE_RollOff_Type
{
	ROLL_OFF_0_03 = 0,	//roll-off = 0.03
	ROLL_OFF_0_10,		//roll-off = 0.10
	ROLL_OFF_0_15,		//roll-off = 0.15
	ROLL_OFF_0_20,		//roll-off = 0.2
	ROLL_OFF_0_25,		//roll-off = 0.25
	ROLL_OFF_0_30,		//roll-off = 0.3
	ROLL_OFF_0_35,		//roll-off = 0.35
	ROLL_OFF_0_40,		//roll-off = 0.4
}R898_SATELLITE_RollOff_Type;

typedef enum _R898_SATELLITE_ClockOut_Type
{
	ClockOutOn = TRUE,
	ClockOutOff= FALSE
}R898_SATELLITE_ClockOut_Type;

typedef enum _R898_SATELLITE_3_5dB_Type
{
	FINEGAIN2_0DB = 0,
	FINEGAIN2_3_5DB
}R898_SATELLITE_3_5dB_Type;

typedef enum _R898_SATELLITE_6dB_Type
{
	FINEGAIN1_0DB = 0,
	FINEGAIN1_6DB
}R898_SATELLITE_6dB_Type;

typedef enum _R898_Share_Xtal_Type
{
	R898_NO_SHARE_XTAL = 0,    //single R898
	R898_MASTER_TO_SLAVE_XTAL, //Dual R898
}R898_Share_Xtal_Type;
typedef struct _R898_Set_Info
{
	UINT32                       RF_KHz;
	UINT32						 SATELLITE_BW;
	UINT32                       SymbolRate_Kbps;
	UINT32						 GpioFrequency_KHz;
	R898_Standard_Type           R898_Standard;

	R898_SATELLITE_OutputSignal_Type  R898_SATELLITE_OutputSignal_Mode;
	R898_SATELLITE_AGC_Type           R898_SATELLITE_AGC_Mode; 
	R898_SATELLITE_AttenVga_Type	  R898_SATELLITE_AttenVga_Mode;
	R898_SATELLITE_FineGain_Type	  R898_SATELLITE_FineGain;
	R898_SATELLITE_RollOff_Type		  R898_SATELLITE_RollOff_Mode;	
	R898_SATELLITE_ClockOut_Type	  R898_SATELLITE_ClockOut_Mode;
	R898_SATELLITE_6dB_Type			  R898_SATELLITE_6dB;
	R898_SATELLITE_3_5dB_Type		  R898_SATELLITE_3_5dB;
}R898_Set_Info;


typedef enum _R898_S_Initial_Type
{
	Init_Freq_Under2G = 0,	//Blind Scan 
	Init_Freq_Over2G,	//Normal(Default)
	Init_Freq_First,
}R898_S_Initial_Type;


typedef struct _R898_RF_Gain_Info
{
	UINT16   RF_gain_comb;
	UINT8   RF_gain1;
	UINT8   RF_gain2;
	UINT8   RF_gain3;
	UINT8   RF_gain4;
}R898_RF_Gain_Info;



typedef struct _R898_S_RF_Gain_Info
{
	UINT8   RF_gain;
}R898_S_RF_Gain_Info;


typedef struct _R898_S_MIXER_Gain_Info
{
	UINT8   MIXER_gain;
}R898_S_MIXER_Gain_Info;


typedef struct _R898_S_LNA_Gain_Info
{
	UINT8   LNA_gain;
}R898_S_LNA_Gain_Info;

//----------------------------------------------------------//
//                   R898 PRIVATE Function                       //
//----------------------------------------------------------//

R898_ErrCode R898_S_PLL(R898_TUNER_NUM R898_Tuner_Num,UINT32 Freq);

//----------------------------------------------------------//
//                   R898 Public Function                       //
//----------------------------------------------------------//
#define R898_Delay_MS(R898_Tuner_Num, x_ms)   	Sleep(x_ms)

R898_ErrCode R898_Init(R898_TUNER_NUM R898_Tuner_Num);
R898_ErrCode R898_SetPllData(R898_TUNER_NUM R898_Tuner_Num, R898_Set_Info R898_INFO);
R898_ErrCode R898_T_Standby(R898_TUNER_NUM R898_Tuner_Num);
R898_ErrCode R898_S_Standby(R898_TUNER_NUM R898_Tuner_Num);
R898_ErrCode R898_T_Wakeup(R898_TUNER_NUM R898_Tuner_Num);
R898_ErrCode R898_GetRfGain(R898_TUNER_NUM R898_Tuner_Num, R898_RF_Gain_Info *pR898_rf_gain);
R898_ErrCode R898_GetRfRssi(R898_TUNER_NUM R898_Tuner_Num, UINT32 RF_Freq_Khz, R898_Standard_Type RT_Standard, INT32 *RfLevelDbm, UINT8 *fgRfMaxGain);
R898_ErrCode R898_RfGainMode(R898_TUNER_NUM R898_Tuner_Num, R898_RF_Gain_TYPE R848_RfGainType);


int poes();

#endif