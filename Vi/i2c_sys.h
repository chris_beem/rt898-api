#ifndef _I2C_SYS_H_
#define _I2C_SYS_H_

#include "R898.h"

typedef struct _I2C_LEN_TYPE
{
	UINT8 RegAddr;
	UINT8 I2cAddr;
	UINT8 Data[256];
	UINT8 Len;
}I2C_LEN_TYPE;

typedef struct _I2C_TYPE
{
	UINT8 RegAddr;
	UINT8 I2cAddr;
	UINT8 Data;
}I2C_TYPE;

bool I2C_Write_Len(R898_TUNER_NUM R898_Tuner_Num, I2C_LEN_TYPE *I2C_Info);
bool I2C_Read_Len(R898_TUNER_NUM R898_Tuner_Num, I2C_LEN_TYPE *I2C_Info);
bool I2C_Write(R898_TUNER_NUM R898_Tuner_Num, I2C_TYPE *I2C_Info);
int R898_Convert(int InvertNum);

#endif